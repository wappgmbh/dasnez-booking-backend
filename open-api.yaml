openapi: 3.0.2
servers:
  - url: 'http://localhost:8080/api/v1/'
    description: Development
  - url: 'https://backend.dasnez.de/api/v1/'
    description: Live
info:
  description: 'daznez API'
  version: 1.1.0
  title: daznez API
  contact:
    email: info@wapp.gmbh
    name: WAPP GmbH
    url: 'https://wapp.gmbh'

paths:
  /accounts:
    get:
      operationId: getAccounts
      tags: [ Accounts ]
      security:
        - TokenAuth: [ ]
        - UserId: [ ]
      parameters:
        - in: query
          name: params
          required: true
          schema:
            type: object
            additionalProperties:
              type: string
        - $ref: '#/components/parameters/paginationParamDraw'
        - $ref: '#/components/parameters/paginationParamStart'
        - $ref: '#/components/parameters/paginationParamLength'
        - $ref: '#/components/parameters/paginationParamSearchValue'
        - $ref: '#/components/parameters/paginationParamOrderColumn'
        - $ref: '#/components/parameters/paginationParamOrderDir'
      responses:
        "200":
          description: List of accounts
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/AccountPaginated'
        default:
          $ref: '#/components/responses/DefaultErrorResponse'

  /accounts/me/isAdmin:
    get:
      operationId: amIAdmin
      tags: [ Accounts ]
      security:
        - TokenAuth: [ ]
        - UserId: [ ]
      responses:
        "200":
          description: True if the current user has admin privileges
          content:
            application/json:
              schema:
                type: boolean
  /districts:
    get:
      operationId: readDistricts
      tags: [ District ]
      responses:
        "200":
          description: All available districts
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/District'

  /offers:
    get:
      operationId: readOffersPaginated
      tags: [ Offer ]
      parameters:
        - in: query
          name: params
          required: true
          schema:
            type: object
            additionalProperties:
              type: string
        - $ref: '#/components/parameters/paginationParamDraw'
        - $ref: '#/components/parameters/paginationParamStart'
        - $ref: '#/components/parameters/paginationParamLength'
        - $ref: '#/components/parameters/paginationParamSearchValue'
        - $ref: '#/components/parameters/paginationParamOrderColumn'
        - $ref: '#/components/parameters/paginationParamOrderDir'
        - in: query
          name: districtId
          required: false
          schema:
            $ref: '#/components/schemas/DbId'
      responses:
        "200":
          description: List of offers
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OfferPaginated'
        default:
          $ref: '#/components/responses/DefaultErrorResponse'

    post:
      operationId: createOffer
      description: |
        MISSING_LICENSE: You do not have a license to ride rickshaws
      tags: [ Offer ]
      security:
        - TokenAuth: [ ]
        - UserId: [ ]
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/OfferPayload'
      responses:
        "200":
          description: Offer has been created
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Offer'
        "409":
          description: Conflict

  /offers/booked:
    get:
      operationId: readBookedOffers
      parameters:
        - in: query
          name: params
          required: true
          schema:
            type: object
            additionalProperties:
              type: string
        - $ref: '#/components/parameters/paginationParamDraw'
        - $ref: '#/components/parameters/paginationParamStart'
        - $ref: '#/components/parameters/paginationParamLength'
        - $ref: '#/components/parameters/paginationParamSearchValue'
        - $ref: '#/components/parameters/paginationParamOrderColumn'
        - $ref: '#/components/parameters/paginationParamOrderDir'
      responses:
        "200":
          description: List of booked offers
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/OfferDetailedPaginated"
        default:
          $ref: '#/components/responses/DefaultErrorResponse'

  /offers/delete/{id}:
    parameters:
      - $ref: '#/components/parameters/idPathParam'
    get:
      operationId: deleteOffer
      tags: [ Offer ]
      parameters:
        - in: query
          name: deleteToken
          required: true
          schema:
            type: string
      responses:
        "200":
          description: Offer was deleted
          content:
            text/html:
              schema:
                type: string
        default:
          $ref: '#/components/responses/DefaultErrorResponse'

  /offers/{id}:
    parameters:
      - $ref: '#/components/parameters/idPathParam'
    delete:
      operationId: cancelOffer
      tags: [ Offer ]
      security:
        - TokenAuth: [ ]
        - UserId: [ ]
      responses:
        "200":
          description: Offer deleted
        "401":
          description: Unauthorized
        "403":
          description: Forbidden
        "404":
          description: Offer not fonud

  /offers/{id}/reserve:
    parameters:
      - $ref: '#/components/parameters/idPathParam'
    post:
      operationId: reserveOffer
      description: |
        NO_RICKSHAW_AVAILABLE: No rickshaw available during the specified time frame
      tags: [ Offer ]
      security:
        - TokenAuth: [ ]
        - UserId: [ ]
      responses:
        "200":
          description: Reservation successful
        "409":
          description: Conflict
        default:
          $ref: '#/components/responses/DefaultErrorResponse'

  /pilots:
    post:
      operationId: addPilot
      description: |
        DUPLICATE_EMAIL: This pilot is already registered
      security:
        - TokenAuth: [ ]
        - UserId: [ ]
      tags: [ Pilot ]
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/PilotPayload"
      responses:
        "200":
          description: Account with given email can now offer rickshaw rides
        "409":
          description: Conflict
        default:
          $ref: '#/components/responses/DefaultErrorResponse'
    get:
      operationId: getPilotsPaginated
      security:
        - TokenAuth: [ ]
        - UserId: [ ]
      tags: [ Pilot ]
      parameters:
        - in: query
          name: params
          required: true
          schema:
            type: object
            additionalProperties:
              type: string
        - $ref: '#/components/parameters/paginationParamDraw'
        - $ref: '#/components/parameters/paginationParamStart'
        - $ref: '#/components/parameters/paginationParamLength'
        - $ref: '#/components/parameters/paginationParamSearchValue'
        - $ref: '#/components/parameters/paginationParamOrderColumn'
        - $ref: '#/components/parameters/paginationParamOrderDir'
      responses:
        "200":
          description: Success
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/PilotPaginated"
        default:
          $ref: '#/components/responses/DefaultErrorResponse'

  /pilots/{id}:
    parameters:
      - $ref: "#/components/parameters/idPathParam"
    delete:
      operationId: deletePilot
      security:
        - TokenAuth: [ ]
        - UserId: [ ]
      tags: [ Pilot ]
      responses:
        "200":
          description: Account with given email can no longer offer rickshaw rides
        default:
          $ref: '#/components/responses/DefaultErrorResponse'

  /requests:
    get:
      operationId: readRequestsPaginated
      tags: [ Request ]
      parameters:
        - in: query
          name: params
          required: true
          schema:
            type: object
            additionalProperties:
              type: string
        - $ref: '#/components/parameters/paginationParamDraw'
        - $ref: '#/components/parameters/paginationParamStart'
        - $ref: '#/components/parameters/paginationParamLength'
        - $ref: '#/components/parameters/paginationParamSearchValue'
        - $ref: '#/components/parameters/paginationParamOrderColumn'
        - $ref: '#/components/parameters/paginationParamOrderDir'
        - in: query
          name: districtId
          required: false
          schema:
            $ref: '#/components/schemas/DbId'
      responses:
        "200":
          description: List of requests
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RequestPaginated'
        default:
          $ref: '#/components/responses/DefaultErrorResponse'

    post:
      operationId: createRequest
      tags: [ Request ]
      security:
        - TokenAuth: [ ]
        - UserId: [ ]
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/RequestPayload'
      responses:
        "200":
          description: Request has been created
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Request'

  /requests/booked:
    get:
      operationId: readBookedRequests
      parameters:
        - in: query
          name: params
          required: true
          schema:
            type: object
            additionalProperties:
              type: string
        - $ref: '#/components/parameters/paginationParamDraw'
        - $ref: '#/components/parameters/paginationParamStart'
        - $ref: '#/components/parameters/paginationParamLength'
        - $ref: '#/components/parameters/paginationParamSearchValue'
        - $ref: '#/components/parameters/paginationParamOrderColumn'
        - $ref: '#/components/parameters/paginationParamOrderDir'
      responses:
        "200":
          description: List of booked requests
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/RequestDetailedPaginated"
        default:
          $ref: '#/components/responses/DefaultErrorResponse'

  /requests/delete/{id}:
    parameters:
      - $ref: '#/components/parameters/idPathParam'
    get:
      operationId: deleteRequest
      tags: [ Request ]
      parameters:
        - in: query
          name: deleteToken
          required: true
          schema:
            type: string
      responses:
        "200":
          description: Request was deleted
          content:
            text/html:
              schema:
                type: string
        default:
          $ref: '#/components/responses/DefaultErrorResponse'

  /requests/{id}:
    parameters:
      - $ref: '#/components/parameters/idPathParam'
    delete:
      operationId: cancelRequest
      tags: [ Request ]
      security:
        - TokenAuth: [ ]
        - UserId: [ ]
      responses:
        "200":
          description: Request deleted
        "401":
          description: Unauthorized
        "403":
          description: Forbidden
        "404":
          description: Request not fonud

  /requests/{id}/reserve:
    parameters:
      - $ref: '#/components/parameters/idPathParam'
    post:
      operationId: reserveRequest
      description: |
        NO_RICKSHAW_AVAILABLE: No rickshaw available during the specified time frame
        MISSING_LICENSE: You do not have a license to ride rickshaws
      tags: [ Request ]
      security:
        - TokenAuth: [ ]
        - UserId: [ ]
      responses:
        "200":
          description: Reservation successful
        "409":
          description: Conflict
        default:
          $ref: '#/components/responses/DefaultErrorResponse'

  /rickshaws/availableDuring:
    get:
      operationId: getRickshawAvailabilityDuring
      tags: [ Rickshaw ]
      security:
        - TokenAuth: [ ]
        - UserId: [ ]
      parameters:
        - in: query
          name: start
          required: true
          description: Timestamp in seconds
          schema:
            $ref: "#/components/schemas/Timestamp"
        - in: query
          name: end
          required: true
          description: Timestamp in seconds
          schema:
            $ref: "#/components/schemas/Timestamp"
      responses:
        "200":
          description: Availability status
          content:
            application/json:
              schema:
                type: boolean
        "401":
          description: Unauthorized
        default:
          $ref: '#/components/responses/DefaultErrorResponse'

  /rickshaws/bookingsDuringDay:
    get:
      operationId: getBookingsDuringDay
      tags: [ Rickshaw ]
      security:
        - TokenAuth: [ ]
        - UserId: [ ]
      parameters:
        - in: query
          name: day
          required: true
          description: Timestamp that lies within the requested day
          schema:
            $ref: "#/components/schemas/Timestamp"
      responses:
        "200":
          description: Timeframes during which no rickshaw is available
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/Timeframe"
        "401":
          description: Unauthorized
        default:
          $ref: '#/components/responses/DefaultErrorResponse'


components:
  schemas:
    DbId:
      type: integer
      format: int64

    Timestamp:
      type: integer
      format: int64
      description: 'Unix timestamp - seconds since 1970'

    Timeframe:
      type: object
      properties:
        start:
          $ref: "#/components/schemas/Timestamp"
        end:
          $ref: "#/components/schemas/Timestamp"

    Account:
      type: object
      properties:
        givenName:
          type: string
        surname:
          type: string
        legacyUserId:
          type: string
        email:
          type: string
        phone:
          type: string

    AccountPaginated:
      allOf:
        - $ref: '#/components/schemas/Pagination'
        - type: object
          properties:
            data:
              type: array
              items:
                $ref: '#/components/schemas/Account'
          required:
            - data

    Offer:
      type: object
      required: [ id, start, end, districts, createdBy ]
      properties:
        id:
          $ref: '#/components/schemas/DbId'
        start:
          $ref: '#/components/schemas/Timestamp'
        end:
          $ref: '#/components/schemas/Timestamp'
        districts:
          type: array
          items:
            $ref: '#/components/schemas/District'
        createdFor:
          type: string
        isOwn:
          type: boolean

    OfferPaginated:
      allOf:
        - $ref: '#/components/schemas/Pagination'
        - type: object
          properties:
            data:
              type: array
              items:
                $ref: '#/components/schemas/Offer'
          required:
            - data

    OfferDetailed:
      allOf:
        - $ref: '#/components/schemas/Offer'
        - type: object
          properties:
            createdBy:
              $ref: '#/components/schemas/Account'
            reservedBy:
              $ref: '#/components/schemas/Account'
            confirmedAt:
              $ref: '#/components/schemas/Timestamp'

    OfferDetailedPaginated:
      allOf:
        - $ref: '#/components/schemas/Pagination'
        - type: object
          properties:
            data:
              type: array
              items:
                $ref: '#/components/schemas/OfferDetailed'
          required:
            - data

    Pilot:
      type: object
      properties:
        id:
          $ref: '#/components/schemas/DbId'
        givenName:
          type: string
        surname:
          type: string
        email:
          type: string
        phone:
          type: string

    PilotPaginated:
      allOf:
        - $ref: '#/components/schemas/Pagination'
        - type: object
          properties:
            data:
              type: array
              items:
                $ref: '#/components/schemas/Pilot'
          required:
            - data

    PilotPayload:
      type: object
      required: [ email ]
      properties:
        email:
          type: string

    Request:
      type: object
      required: [ id, start, end, districts, createdFor ]
      properties:
        id:
          $ref: '#/components/schemas/DbId'
        start:
          $ref: '#/components/schemas/Timestamp'
        end:
          $ref: '#/components/schemas/Timestamp'
        districts:
          type: array
          items:
            $ref: '#/components/schemas/District'
        createdFor:
          type: string
        isOwn:
          type: boolean

    RequestPaginated:
      allOf:
        - $ref: '#/components/schemas/Pagination'
        - type: object
          properties:
            data:
              type: array
              items:
                $ref: '#/components/schemas/Request'
          required:
            - data

    RequestDetailed:
      allOf:
        - $ref: '#/components/schemas/Request'
        - type: object
          properties:
            createdBy:
              $ref: '#/components/schemas/Account'
            reservedBy:
              $ref: '#/components/schemas/Account'
            confirmedAt:
              $ref: '#/components/schemas/Timestamp'

    RequestDetailedPaginated:
      allOf:
        - $ref: '#/components/schemas/Pagination'
        - type: object
          properties:
            data:
              type: array
              items:
                $ref: '#/components/schemas/RequestDetailed'
          required:
            - data

    District:
      type: object
      required: [ id, name ]
      properties:
        id:
          $ref: '#/components/schemas/DbId'
        name:
          type: string

    OfferPayload:
      type: object
      required: [ start, end, districtIds ]
      properties:
        start:
          $ref: '#/components/schemas/Timestamp'
        end:
          $ref: '#/components/schemas/Timestamp'
        districtIds:
          type: array
          items:
            $ref: '#/components/schemas/DbId'
        onBehalfOf:
          $ref: '#/components/schemas/PersonPayload'

    RequestPayload:
      type: object
      required: [ start, end, districtIds ]
      properties:
        start:
          $ref: '#/components/schemas/Timestamp'
        end:
          $ref: '#/components/schemas/Timestamp'
        districtIds:
          type: array
          items:
            $ref: '#/components/schemas/DbId'
        onBehalfOf:
          $ref: '#/components/schemas/PersonPayload'

    PersonPayload:
      type: object
      required: [ givenName, surname, hint, phone, email ]
      properties:
        givenName:
          type: string
        surname:
          type: string
        hint:
          type: string
        phone:
          type: string
        email:
          type: string

    Pagination:
      type: object
      properties:
        draw:
          type: integer
          format: int64
          description: The draw counter that this object is a response to - from the draw parameter sent as part of the data request. Note that it is strongly recommended for security reasons that you cast this parameter to an integer, rather than simply echoing back to the client what it sent in the draw parameter, in order to prevent Cross Site Scripting (XSS) attacks.
        recordsTotal:
          type: integer
          format: int64
          description: Total records, before filtering (i.e. the total number of records in the database)
        recordsFiltered:
          type: integer
          format: int64
          description: Total records, after filtering (i.e. the total number of records after filtering has been applied - not just the number of records being returned for this page of data).
        error:
          type: string
          description: If an error occurs during the running of the server-side processing script, you can inform the user of this error by passing back the error message to be displayed using this parameter. Do not include if there is no error.
      required: [ draw, recordsTotal, recordsFiltered ]

    ErrorResponse:
      type: object
      properties:
        timestamp:
          type: string
        status:
          type: integer
        error:
          type: string
        message:
          type: string
        path:
          type: string

  responses:
    DefaultErrorResponse:
      description: 'detailed error response'
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/ErrorResponse'

  parameters:
    idPathParam:
      in: path
      name: id
      schema:
        $ref: '#/components/schemas/DbId'
      required: true
    paginationParamDraw:
      name: draw
      schema:
        type: integer
        format: int64
        default: 0
      in: query
      required: true
      description: Draw counter. This is used by DataTables to ensure that the Ajax returns from server-side processing requests are drawn in sequence by DataTables (Ajax requests are asynchronous and thus can return out of sequence). This is used as part of the draw return parameter (see below).
    paginationParamStart:
      name: start
      schema:
        type: integer
        format: int64
        default: 0
      in: query
      required: true
      description: Paging first record indicator. This is the start point in the current data set (0 index based - i.e. 0 is the first record).
    paginationParamLength:
      schema:
        type: integer
        default: -1
      name: length
      in: query
      required: true
      description: Number of records that the table can display in the current draw. It is expected that the number of records returned will be equal to this number, unless the server has fewer records to return. Note that this can be -1 to indicate that all records should be returned (although that negates any benefits of server-side processing!)
    paginationParamSearchValue:
      name: search[value]
      schema:
        type: string
      required: false
      in: query
      description: Global search value. To be applied to all columns which have searchable as true.
    paginationParamOrderColumn:
      name: order[0][column]
      schema:
        type: integer
      in: query
      required: false
      description: Column to which ordering should be applied. This is an index reference to the columns array of information that is also submitted to the server.
    paginationParamOrderDir:
      name: order[0][dir]
      schema:
        type: string
        enum:
          - asc
          - desc
      required: false
      in: query
      description: Ordering direction for this column. It will be asc or desc to indicate ascending ordering or descending ordering, respectively.

  ##### SECURITY #####
  securitySchemes:
    TokenAuth:
      type: apiKey
      in: header
      name: dasnez-token
    UserId:
      type: apiKey
      in: header
      name: dasnez-user-id


