rootProject.name = "dasnez-booking-backend"

pluginManagement {
    repositories {
        mavenCentral()
        gradlePluginPortal()
    }
}
