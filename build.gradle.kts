import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("org.springframework.boot") version "2.5.12"
	id("io.spring.dependency-management") version "1.0.11.RELEASE"
	kotlin("jvm") version "1.5.10"
	kotlin("plugin.spring") version "1.5.10"
	kotlin("plugin.jpa") version "1.5.10"
	kotlin("kapt") version "1.5.10"
	id("org.openapi.generator") version "5.1.1"
	id("fi.evident.beanstalk") version "0.3.0"
	id("war")
}

group = "de.daznez"
version = "0.1.0-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
	mavenCentral()
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-data-jpa")
	implementation("org.springframework.boot:spring-boot-starter-security")
	implementation("org.springframework.boot:spring-boot-starter-web")
	compileOnly("org.springframework.boot:spring-boot-devtools")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("org.flywaydb:flyway-core")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("io.jsonwebtoken:jjwt:0.9.1")
	implementation("com.samskivert:jmustache:1.15")
	implementation("com.amazonaws:aws-java-sdk-ses:1.11.618")
	implementation("com.amazonaws:aws-java-sdk-s3:1.12.7")
	implementation("org.mapstruct:mapstruct:1.4.2.Final")

	runtimeOnly("org.postgresql:postgresql")

	kapt("org.mapstruct:mapstruct-processor:1.4.2.Final")

	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("org.springframework.security:spring-security-test")
	testImplementation("io.rest-assured:rest-assured:4.1.1")
	testImplementation("io.rest-assured:rest-assured-all:4.1.1")
	testImplementation("org.mockito.kotlin:mockito-kotlin:3.2.0")

	testRuntimeOnly("com.h2database:h2")

	providedRuntime("org.springframework.boot:spring-boot-starter-tomcat")
}

sourceSets["main"].java {
	srcDir(buildDir.resolve("generate-resources/main/src/main/kotlin"))
}

tasks.withType<KotlinCompile> {
	dependsOn(tasks.openApiGenerate)
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "11"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}

openApiGenerate {
	apiPackage.set("de.daznez.api")
	generatorName.set("kotlin-spring")
	inputSpec.set("open-api.yaml")
	modelNameSuffix.set("Dto")
	modelPackage.set("de.daznez.dto")

	configOptions.put("exceptionHandler", "false")
	configOptions.put("enumPropertyNaming", "UPPERCASE")
	configOptions.put("interfaceOnly", "true")
	configOptions.put("modelMutable", "true")
	configOptions.put("useBeanValidation", "false")
}

beanstalk {
	s3Endpoint = "s3-eu-central-1.amazonaws.com"
	beanstalkEndpoint = "elasticbeanstalk.eu-central-1.amazonaws.com"

	deployments {
		register("production") {
			file = tasks.bootWar
			application = "dasnez-booking"
			environment = "Dasnezbooking-env"
		}
	}
}
