package de.daznez.daznez.external

import de.daznez.daznez.external.legacy.LegacyBackendService
import de.daznez.daznez.external.legacy.LegacyDistrict
import de.daznez.daznez.external.legacy.LegacyGroup
import de.daznez.daznez.external.legacy.LegacyUser

class MockLegacyBackendService: LegacyBackendService {

    override fun readUser(userId: String, token: String?): LegacyUser {
        return when (token) {
            "admin" -> LegacyUser().also {
                it.id = userId
                it.username = userId
                it.email = "admin@example.com"
                it.firstname = userId.split(" ").first()
                it.lastname = userId.split(" ").last()
                it.city = "Delbrück"
                it.district = "Anreppen"
                it.street = "Teststraße 1"
                it.zip = "12345"
                it.phone = "004912345789"
                it.birthdate = "1970-01-01"
                it.groups = mutableListOf(LegacyGroup().also { group ->
                    group.groupId = "19156353-870c-46e7-9746-3719ba0abd13"
                    group.groupName = "admin"
                    group.isActive = true
                    group.userId = userId
                })
            }
            "valid" -> LegacyUser().also {
                it.id = userId
                it.username = userId
                it.email = "test@example.com"
                it.firstname = userId.split(" ").first()
                it.lastname = userId.split(" ").last()
                it.city = "Delbrück"
                it.district = "Anreppen"
                it.street = "Teststraße 1"
                it.zip = "12345"
                it.phone = "004912345789"
                it.birthdate = "1970-01-01"
                it.groups = mutableListOf()
            }
        else -> LegacyUser().also {
                it.id = "123456789"
                it.username = userId
                it.firstname = userId.split(" ").first()
                it.lastname = userId.split(" ").last()
                it.city = "Delbrück"
                it.district = "Anreppen"
            }
        }
    }

    override fun readDistricts(): List<LegacyDistrict> {
        return listOf(
            LegacyDistrict().also { it.id = "01"; it.name = "Anreppen" },
            LegacyDistrict().also { it.id = "01"; it.name = "Bentfeld" },
            LegacyDistrict().also { it.id = "01"; it.name = "Boke" },
            LegacyDistrict().also { it.id = "01"; it.name = "Delbrück-Mitte" },
            LegacyDistrict().also { it.id = "01"; it.name = "Hagen" },
            LegacyDistrict().also { it.id = "01"; it.name = "Ostenland" },
            LegacyDistrict().also { it.id = "01"; it.name = "Steinhorst" },
            LegacyDistrict().also { it.id = "01"; it.name = "Schöning" },
            LegacyDistrict().also { it.id = "01"; it.name = "Westenholz" },
            LegacyDistrict().also { it.id = "01"; it.name = "Lippling" }
        )
    }
}