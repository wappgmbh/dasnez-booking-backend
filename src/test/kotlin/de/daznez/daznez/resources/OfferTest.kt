package de.daznez.daznez.resources

import de.daznez.daznez.BaseTest
import de.daznez.daznez.DataCreator
import de.daznez.daznez.entity.Account
import de.daznez.daznez.entity.Offer
import de.daznez.daznez.entity.Pilot
import de.daznez.daznez.external.mail.MailService
import de.daznez.daznez.repository.OfferRepository
import de.daznez.daznez.repository.PilotRepository
import de.daznez.dto.OfferDto
import de.daznez.dto.OfferPaginatedDto
import de.daznez.dto.OfferPayloadDto
import io.restassured.RestAssured.given
import io.restassured.http.ContentType
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.Matchers
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.mockito.kotlin.argumentCaptor
import org.mockito.kotlin.verify
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import java.sql.Timestamp
import java.time.LocalDateTime
import javax.transaction.Transactional

class OfferTest @Autowired constructor(
    private val dataCreator: DataCreator,
    private val offerRepository: OfferRepository,
    private val pilotRepository: PilotRepository
): BaseTest() {

    @MockBean
    lateinit var mailServiceMock: MailService

    @Test
    fun readOffers() {
        val offer1 = dataCreator.saveOffer(inDays = 3)
        dataCreator.saveOffer(inDays = 4)
        // Will be blocked be reserved offer
        dataCreator.saveOffer(inDays = 2)
        val offer2 = dataCreator.saveOffer(inDays = 1)
        dataCreator.saveOffer(inDays = 2, reservedBy = dataCreator.saveOrFetchAccount("Other User"))

        val responseBody = given()
            .queryParam("draw", "851")
            .queryParam("start", 1)
            .queryParam("length", 2)
            .get("/api/v1/offers")
            .then().statusCode(200)
            .extract().body().`as`(OfferPaginatedDto::class.java)

        assertThat(responseBody.draw).isEqualTo(851)
        assertThat(responseBody.recordsTotal).isEqualTo(3)
        assertThat(responseBody.recordsFiltered).isEqualTo(3)
        assertThat(responseBody.error).isNull()
        assertThat(responseBody.data).hasSize(2)
        assertThat(responseBody.data[0].id).isEqualTo(offer1.id)
        assertThat(responseBody.data[0].start).isEqualTo(offer1.start)
        assertThat(responseBody.data[0].end).isEqualTo(offer1.end)
        assertThat(responseBody.data[0].districts.map { it.name }).isEqualTo(offer1.districts.map { it.name })
        assertThat(responseBody.data[0].createdFor).isEqualTo(offer1.createdBy.getAnonName())
        assertThat(responseBody.data[0].isOwn).isFalse
        assertThat(responseBody.data[1].id).isEqualTo(offer2.id)
    }

    @Test
    fun readOffersAuthenticated() {
        val offer1 = dataCreator.saveOffer(createdByFullName = "Test User", inDays = 2)
        val offer2 = dataCreator.saveOffer(createdByFullName = "Other User", inDays = 1)

        val responseBody = requestAs("Test User")
            .get("/api/v1/offers")
            .then().statusCode(200)
            .extract().body().`as`(OfferPaginatedDto::class.java)

        assertThat(responseBody.data[0].id).isEqualTo(offer1.id)
        assertThat(responseBody.data[0].isOwn).isTrue
        assertThat(responseBody.data[1].id).isEqualTo(offer2.id)
        assertThat(responseBody.data[1].isOwn).isFalse
    }

    @Disabled
    @Test
    @Transactional
    fun createOffer() {
        val start = LocalDateTime.now().withNano(0)
        val end = start.plusHours(2)
        val districts = dataCreator.fetchDistricts()
        pilotRepository.save(Pilot(email = "test@example.com"))

        val responseBody = requestAs("Offer Creator")
            .contentType(ContentType.JSON)
            .body(OfferPayloadDto(
                start = Timestamp.valueOf(start),
                end = Timestamp.valueOf(end),
                districtIds = districts.map { it.id }
            ))
            .post("/api/v1/offers")
            .then().statusCode(200)
            .extract().body().`as`(OfferDto::class.java)

        transaction {
            val savedOffer = offerRepository.findById(responseBody.id).orElseThrow()
            assertThat(savedOffer.start).isEqualTo(Timestamp.valueOf(start))
            assertThat(savedOffer.end).isEqualTo(Timestamp.valueOf(end))
            assertThat(savedOffer.districts.map { it.id }).isEqualTo(districts.map { it.id })
            assertThat(savedOffer.createdBy.givenName).isEqualTo("Offer")
            assertThat(savedOffer.createdBy.surname).isEqualTo("Creator")

            assertThat(responseBody.start).isEqualTo(Timestamp.valueOf(start))
            assertThat(responseBody.end).isEqualTo(Timestamp.valueOf(end))
            assertThat(responseBody.districts.map { it.name }).isEqualTo(districts.map { it.name })
            assertThat(responseBody.createdFor).isEqualTo("Offer C")
        }
    }

    @Test
    fun createOfferWithoutAuth() {
        val start = LocalDateTime.now().withNano(0)
        val end = start.plusHours(2)
        val districts = dataCreator.fetchDistricts()

        given()
            .contentType(ContentType.JSON)
            .body(OfferPayloadDto(
                start = Timestamp.valueOf(start),
                end = Timestamp.valueOf(end),
                districtIds = districts.map { it.id }
            ))
            .post("/api/v1/offers")
            .then().statusCode(401)
    }

    @Test
    fun reserveOffer() {
        val offer = dataCreator.saveOffer()

        requestAs("Offer Acceptor")
            .post("/api/v1/offers/${offer.id}/reserve")
            .then().statusCode(200)

        val offerCaptor = argumentCaptor<Offer>()
        val acceptedByCaptor = argumentCaptor<Account>()
        verify(mailServiceMock).sendOfferReservedMail(offerCaptor.capture(), acceptedByCaptor.capture())
        val mailOffer = offerCaptor.firstValue
        val mailAcceptedBy = acceptedByCaptor.firstValue
        assertThat(mailOffer.id).isEqualTo(offer.id)
        assertThat(mailAcceptedBy.legacyUserId).isEqualTo("Offer Acceptor")
    }

    @Test
    fun reserveOfferWithoutAuth() {
        val offer = dataCreator.saveOffer()

        given()
            .post("/api/v1/offers/${offer.id}/reserve")
            .then().statusCode(401)
    }

    @Test
    fun deleteOffer() {
        val reservedBy = dataCreator.saveOrFetchAccount("Reserved By")
        val offer = dataCreator.saveOffer(reservedBy = reservedBy)

        given()
            .queryParam("deleteToken", offer.deleteToken)
            .get("/api/v1/offers/delete/${offer.id}")
            .then().statusCode(200)
            .header("Content-Type", Matchers.startsWith("text/html"))

        val offerCaptor = argumentCaptor<Offer>()
        verify(mailServiceMock).sendOfferConfirmedMail(offerCaptor.capture())
        assertThat(offerCaptor.firstValue.id).isEqualTo(offer.id)

        transaction {
            val savedOffer = offerRepository.findById(offer.id).orElseThrow()
            assertThat(savedOffer.confirmedAt).isNotNull
        }
    }

    @Test
    fun deleteOfferWithInvalidDeleteToken() {
        val offer = dataCreator.saveOffer()

        given()
            .queryParam("deleteToken", "123456789")
            .get("/api/v1/offers/delete/${offer.id}")
            .then().statusCode(401)

        assertThat(offerRepository.existsById(offer.id)).isTrue
    }

    @Test
    fun deleteOfferWithoutDeleteToken() {
        val offer = dataCreator.saveOffer()

        given()
            .get("/api/v1/offers/delete/${offer.id}")
            .then().statusCode(400)

        assertThat(offerRepository.existsById(offer.id)).isTrue
    }

    @Test
    fun cancelOffer() {
        val offer = dataCreator.saveOffer(createdByFullName = "Test User")

        requestAs("Test User")
            .delete("/api/v1/offers/${offer.id}")
            .then().statusCode(200)

        assertThat(offerRepository.findById(offer.id)).isEmpty
    }

    @Test
    fun cancelOfferAsAdmin() {
        val offer = dataCreator.saveOffer(createdByFullName = "Test User")
        requestAsAdmin("Admin User")
            .delete("/api/v1/offers/${offer.id}")
            .then().statusCode(200)

        assertThat(offerRepository.findById(offer.id)).isEmpty
    }

    @Test
    fun cancelOfferWithoutAuth() {
        val offer = dataCreator.saveOffer(createdByFullName = "Test User")

        given()
            .delete("/api/v1/offers/${offer.id}")
            .then().statusCode(401)

        assertThat(offerRepository.findById(offer.id)).isPresent
    }

    @Test
    fun cancelOfferAsOtherUser() {
        val offer = dataCreator.saveOffer(createdByFullName = "Test User")

        requestAs("Other User")
            .delete("/api/v1/offers/${offer.id}")
            .then().statusCode(403)

        assertThat(offerRepository.findById(offer.id)).isPresent
    }
}