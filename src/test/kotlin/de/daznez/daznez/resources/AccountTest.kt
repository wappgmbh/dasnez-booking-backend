package de.daznez.daznez.resources

import de.daznez.daznez.BaseTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class AccountTest: BaseTest() {

    @Test
    fun amIAdminAsAdmin() {
        val isAdmin = requestAsAdmin("Admin User")
            .get("/api/v1/accounts/me/isAdmin")
            .then().statusCode(200)
            .extract().body().`as`(Boolean::class.java)

        assertThat(isAdmin).isTrue    }

    @Test
    fun amIAdminAsNonAdmin() {
        val isAdmin = requestAs("Test User")
            .get("/api/v1/accounts/me/isAdmin")
            .then().statusCode(200)
            .extract().body().`as`(Boolean::class.java)

        assertThat(isAdmin).isFalse
    }
}