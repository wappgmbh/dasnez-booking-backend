package de.daznez.daznez.resources

import de.daznez.daznez.BaseTest
import de.daznez.dto.DistrictDto
import io.restassured.RestAssured.given
import io.restassured.common.mapper.TypeRef
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

class DistrictTest @Autowired constructor(

): BaseTest() {

    @Test
    fun readDistricts() {
        val responseBody = given()
            .get("/api/v1/districts")
            .then().statusCode(200)
            .extract().body().`as`(object: TypeRef<List<DistrictDto>>() {})

        assertThat(responseBody).hasSize(10)
        assertThat(responseBody.singleOrNull { it.name == "Anreppen" }).isNotNull
        assertThat(responseBody.singleOrNull { it.name == "Delbrück-Mitte" }).isNotNull
    }
}