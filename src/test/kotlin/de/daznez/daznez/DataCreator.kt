package de.daznez.daznez

import de.daznez.daznez.entity.*
import de.daznez.daznez.repository.*
import org.springframework.stereotype.Service
import java.sql.Timestamp
import java.time.LocalDateTime
import java.util.*

@Service
class DataCreator(
    private val districtRepository: DistrictRepository,
    private val accountRepository: AccountRepository,
    private val personRepository: PersonRepository,
    private val offerRepository: OfferRepository,
    private val requestRepository: RequestRepository,
    private val testUtil: TestUtil
) {

    fun saveOrFetchAccount(fullName: String): Account {
        val existing = accountRepository.findByLegacyUserId(fullName)
        if (existing.isPresent) {
            return existing.get()
        }
        return accountRepository.save(Account(
            givenName = fullName.split(" ").first(),
            surname = fullName.split(" ").last(),
            legacyUserId = fullName,
            email = "$fullName@example.com",
            phone = "123456789"
        ))
    }

    fun fetchDistrict(): District {
        return districtRepository.findByName("Anreppen").orElseThrow()
    }

    fun fetchDistricts(): List<District> {
        return listOf(
            districtRepository.findByName("Anreppen").orElseThrow(),
            districtRepository.findByName("Delbrück-Mitte").orElseThrow()
        )
    }

    fun saveOffer(
        hour: Int = 12,
        inDays: Long = 1,
        durationHours: Long = 1,
        districts: List<District> = fetchDistricts(),
        createdByFullName: String = "Test User",
        reservedBy: Account? = null
    ): Offer {
        val start = LocalDateTime.now()
            .plusDays(inDays)
            .withHour(hour)
            .withMinute(0)
            .withSecond(0)
            .withNano(0)
        val end = start.plusHours(durationHours)
        val createdBy = saveOrFetchAccount(createdByFullName)
        return offerRepository.save(
            Offer(
                start = Timestamp.valueOf(start),
                end = Timestamp.valueOf(end),
                districts = districts,
                createdBy = createdBy,
                deleteToken = UUID.randomUUID().toString(),
                reservedBy = reservedBy
            )
        )
    }

    fun savePerson(
        givenName: String = "Test",
        surname: String = "Person",
        location: String = "In the middle of nowhere",
        district: District = fetchDistrict(),
        phone: String = "456789",
        email: String = "person@example.com"
    ): Person {
        return personRepository.save(Person(
            givenName = givenName,
            surname = surname,
            hint = location,
            phone = phone,
            email = email
        ))
    }

    fun saveRequest(
        hour: Int = 12,
        inDays: Long = 1,
        durationHours: Long = 1,
        districts: List<District> = fetchDistricts(),
        createdByFullName: String = "Test User",
        createdFor: Person? = null,
        reservedBy: Account? = null
    ): Request {
        val start = LocalDateTime.now()
            .plusDays(inDays)
            .withHour(hour)
            .withMinute(0)
            .withSecond(0)
            .withNano(0)
        val end = start.plusHours(durationHours)
        val createdBy = saveOrFetchAccount(createdByFullName)
        return requestRepository.save(
            Request(
                start = Timestamp.valueOf(start),
                end = Timestamp.valueOf(end),
                districts = districts,
                createdBy = createdBy,
                createdFor = createdFor,
                deleteToken = UUID.randomUUID().toString(),
                reservedBy = reservedBy
            )
        )
    }
}