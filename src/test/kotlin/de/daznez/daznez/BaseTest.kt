package de.daznez.daznez

import io.restassured.RestAssured
import io.restassured.config.EncoderConfig.encoderConfig
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.context.annotation.Import
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig

@SpringJUnitConfig
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@Import(TestConfiguration::class)
abstract class BaseTest {

    @LocalServerPort
    private var port: Int = 0

    @Autowired
    protected lateinit var util: TestUtil

    @BeforeEach
    fun init() {
        RestAssured.baseURI = "http://localhost:${port}"
        RestAssured.config = RestAssured.config()
            .encoderConfig(encoderConfig()
                .appendDefaultContentCharsetToContentTypeIfUndefined(false))
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails()
        util.clearDb()
    }

    fun requestAs(fullName: String) = util.requestAs(fullName)

    fun requestAsAdmin(fullName: String) = util.requestAsAdmin(fullName)

    fun <T> transaction(action: () -> T) = util.transaction(action)
}