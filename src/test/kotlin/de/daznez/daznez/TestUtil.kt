package de.daznez.daznez

import de.daznez.daznez.repository.*
import io.restassured.RestAssured
import io.restassured.specification.RequestSpecification
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import javax.transaction.Transactional

@Component
class TestUtil(
    private val accountRepository: AccountRepository,
    private val personRepository: PersonRepository,
    private val offerRepository: OfferRepository,
    private val requestRepository: RequestRepository,
    @Value("\${security.auth.header.userId}")
    var userIdHeaderName: String,
    @Value("\${security.auth.header.token}")
    var tokenHeaderName: String,
) {

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    fun clearDb() {
        offerRepository.deleteAll()
        requestRepository.deleteAll()
        personRepository.deleteAll()
        accountRepository.deleteAll()
    }

    fun requestAs(fullName: String): RequestSpecification {
        return RestAssured.given()
            .header(userIdHeaderName, fullName)
            .header(tokenHeaderName, "valid")
    }

    fun requestAsAdmin(fullName: String): RequestSpecification {
        return RestAssured.given()
            .header(userIdHeaderName, fullName)
            .header(tokenHeaderName, "admin")
    }

    /**
     * Wrap db code using REQUIRES_NEW to ensure that
     * the changes are visible to the tested application
     */
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    fun <T> transaction(action: () -> T): T {
        return action()
    }
}