package de.daznez.daznez

import de.daznez.daznez.external.legacy.LegacyBackendService
import de.daznez.daznez.external.MockLegacyBackendService
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean

@TestConfiguration
class TestConfiguration {

    @Bean
    fun legacyBackendService(): LegacyBackendService {
        return MockLegacyBackendService()
    }
}