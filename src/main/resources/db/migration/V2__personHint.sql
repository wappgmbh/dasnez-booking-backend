ALTER TABLE person RENAME location TO hint;
ALTER TABLE request ADD COLUMN reserved_by_id BIGINT REFERENCES account(id);
ALTER TABLE offer ADD COLUMN reserved_by_id BIGINT REFERENCES account(id);