ALTER TABLE offer_has_district
    DROP CONSTRAINT offer_has_district_offer_id_fkey,
    ADD CONSTRAINT offer_has_district_offer_id_fkey FOREIGN KEY (offer_id) REFERENCES offer(id) ON DELETE CASCADE;

ALTER TABLE request_has_district
    DROP CONSTRAINT request_has_district_request_id_fkey,
    ADD CONSTRAINT request_has_district_request_id_fkey FOREIGN KEY (request_id) REFERENCES request(id) ON DELETE CASCADE;
