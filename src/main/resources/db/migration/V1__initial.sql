CREATE TABLE account(
    id BIGSERIAL PRIMARY KEY,
    given_name VARCHAR(255) NOT NULL,
    surname VARCHAR(255) NOT NULL,
    legacy_user_id VARCHAR(36) NOT NULL UNIQUE,
    email VARCHAR(255) NOT NULL UNIQUE,
    phone VARCHAR(255) NOT NULL
);

CREATE TABLE district(
    id BIGSERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);

CREATE TABLE person(
    id BIGSERIAL PRIMARY KEY,
    given_name VARCHAR(255) NOT NULL,
    surname VARCHAR(255) NOT NULL,
    location VARCHAR(255) NOT NULL,
    district_id BIGINT NOT NULL REFERENCES district(id),
    phone VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL
);

CREATE TABLE offer(
    id BIGSERIAL PRIMARY KEY,
    start TIMESTAMP NOT NULL,
    "end" TIMESTAMP NOT NULL,
    created_by_id BIGINT NOT NULL REFERENCES account(id),
    delete_token VARCHAR(36) NOT NULL
);

CREATE TABLE offer_has_district(
    offer_id BIGINT NOT NULL REFERENCES offer(id),
    district_id BIGINT NOT NULL REFERENCES district(id)
);

CREATE TABLE request(
    id BIGSERIAL PRIMARY KEY,
    start TIMESTAMP NOT NULL,
    "end" TIMESTAMP NOT NULL,
    created_by_id BIGINT NOT NULL REFERENCES account(id),
    created_for_id BIGINT REFERENCES person(id),
    delete_token VARCHAR(36) NOT NULL
);

CREATE TABLE request_has_district(
    request_id BIGINT NOT NULL REFERENCES request(id),
    district_id BIGINT NOT NULL REFERENCES district(id)
);