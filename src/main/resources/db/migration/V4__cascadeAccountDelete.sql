ALTER TABLE offer
    DROP CONSTRAINT offer_created_by_id_fkey,
    ADD CONSTRAINT offer_created_by_id_fkey FOREIGN KEY (created_by_id) REFERENCES account(id) ON DELETE CASCADE;

ALTER TABLE offer
    DROP CONSTRAINT offer_reserved_by_id_fkey,
    ADD CONSTRAINT offer_reserved_by_id_fkey FOREIGN KEY (reserved_by_id) REFERENCES account(id) ON DELETE SET NULL;

ALTER TABLE request
    DROP CONSTRAINT request_created_by_id_fkey,
    ADD CONSTRAINT request_created_by_id_fkey FOREIGN KEY (created_by_id) REFERENCES account(id) ON DELETE CASCADE;

ALTER TABLE request
    DROP CONSTRAINT request_reserved_by_id_fkey,
    ADD CONSTRAINT request_reserved_by_id_fkey FOREIGN KEY (reserved_by_id) REFERENCES account(id) ON DELETE SET NULL;
