CREATE TABLE pilot (
    id BIGSERIAL PRIMARY KEY,
    email VARCHAR(255) NOT NULL
);
CREATE UNIQUE INDEX pilot_email ON pilot(email);