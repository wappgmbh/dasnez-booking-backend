ALTER TABLE offer
    ADD COLUMN confirmed_at TIMESTAMP;

ALTER TABLE request
    ADD COLUMN confirmed_at TIMESTAMP;
