package de.daznez.daznez.service

import de.daznez.daznez.entity.Account
import de.daznez.daznez.external.legacy.LegacyUser
import de.daznez.daznez.repository.AccountRepository
import org.springframework.stereotype.Service

@Service
class AccountService(
    private val accountRepository: AccountRepository
) {

    fun findForLegacyUser(legacyUser: LegacyUser): Account {
        val existing = accountRepository.findByLegacyUserId(legacyUser.id)
        if (existing.isPresent) {
            return existing.get()
        }

        return accountRepository.save(Account(
            legacyUserId = legacyUser.id,
            givenName = legacyUser.firstname,
            surname = legacyUser.lastname,
            email = legacyUser.email!!,
            phone = legacyUser.phone!!
        ))
    }
}