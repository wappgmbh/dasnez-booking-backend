package de.daznez.daznez.service

import de.daznez.daznez.entity.Account
import de.daznez.daznez.external.legacy.LegacyBackendService
import de.daznez.daznez.external.mail.MailService
import de.daznez.daznez.legacy.repository.UserRepository
import de.daznez.daznez.repository.AccountRepository
import de.daznez.daznez.repository.OfferRepository
import de.daznez.daznez.repository.PilotRepository
import de.daznez.daznez.repository.RequestRepository
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.sql.Timestamp
import java.time.Instant
import java.util.*
import javax.transaction.Transactional

@Service
class CronService(
    private val accountRepository: AccountRepository,
    private val offerRepository: OfferRepository,
    private val requestRepository: RequestRepository,
    private val legacyBackendService: LegacyBackendService,
    private val pilotRepository: PilotRepository,
    private val userRepository: UserRepository,
    private val mailService: MailService
) {

    private val logger = LoggerFactory.getLogger(CronService::class.java)

    @Scheduled(cron = "0 43 * * * *")
    fun scheduledRemoveDeletedAccounts() {
        logger.info("Starting to delete removed accounts")
        for (account in accountRepository.findAll()) {
            if (legacyBackendService.readUser(account.legacyUserId, null) == null) {
                deleteAccount(account)
            }
        }
    }

    @Scheduled(cron = "0 11,41 * * * *")
    @Transactional
    fun updateAccounts() {
        val accounts = accountRepository.findAll()
        logger.info("Updating data from ${accounts.size} accounts")
        val legacyIds = accounts.mapNotNull {
            try {
                UUID.fromString(it.legacyUserId)
            } catch (ex: Exception) {
                logger.warn("Invalid legacy user id ${it.legacyUserId}")
                null
            } }
        val legacyUsers = userRepository.findAllById(legacyIds)
        logger.info("Loaded ${legacyUsers.size} users from legacy db")
        val accountsByLegacyId = accounts.associateBy { it.legacyUserId }
        val accountsToSave = legacyUsers.mapNotNull { legacyUser ->
            val account = accountsByLegacyId[legacyUser.id.toString()]!!
            if (account.email != legacyUser.email || account.phone != legacyUser.phone) {
                account.email = legacyUser.email ?: ""
                account.phone = legacyUser.phone ?: ""
                account
            } else {
                null
            }
        }
        accountRepository.saveAll(accountsToSave)
        logger.info("${accountsToSave.size} accounts have been updated")
    }

    @Transactional
    private fun deleteAccount(account: Account) {
        try {
            val now = Timestamp.from(Instant.now())
            for (offer in offerRepository.confirmedOffersCreatedByAndAfter(account, now)) {
                mailService.sendAccountDeletedMail(offer.reservedBy!!.email, account, offer)
            }
            for (offer in offerRepository.confirmedOffersReservedByAndAfter(account, now)) {
                mailService.sendAccountDeletedMail(offer.createdForEmail, account, offer)
            }
            for (request in requestRepository.confirmedRequestsCreatedByAndAfter(account, now)) {
                mailService.sendAccountDeletedMail(request.reservedBy!!.email, account, request)
            }
            for (request in requestRepository.confirmedRequestsReservedByAndAfter(account, now)) {
                mailService.sendAccountDeletedMail(request.createdForEmail, account, request)
            }
            pilotRepository.findByEmail(account.email).ifPresent {
                pilotRepository.delete(it)
            }
            accountRepository.delete(account)
        } catch (ex: Exception) {
            logger.warn("Failed to delete account ${account.email}", ex)
        }
    }
}