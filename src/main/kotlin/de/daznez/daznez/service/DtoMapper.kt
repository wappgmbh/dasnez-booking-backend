package de.daznez.daznez.service

import de.daznez.daznez.entity.*
import de.daznez.daznez.external.legacy.LegacyUser
import de.daznez.daznez.model.PilotDetails
import de.daznez.daznez.pagination.PaginationObject
import de.daznez.dto.*
import org.springframework.stereotype.Service

@Service
class DtoMapper {

    fun requestToDto(request: Request, user: LegacyUser?): RequestDto {
        return RequestDto(
            id = request.id,
            start = request.start,
            end = request.end,
            createdFor = request.createdFor?.getAnonName() ?: request.createdBy.getAnonName(),
            districts = request.districts.map(this::districtToDto),
            isOwn = request.createdBy.legacyUserId == user?.id
        )
    }

    fun paginatedRequestsToDto(requests: PaginationObject<Request>, user: LegacyUser?): RequestPaginatedDto {
        return RequestPaginatedDto(
            draw = requests.draw.toLong(),
            recordsTotal = requests.recordsTotal,
            recordsFiltered = requests.recordsFiltered,
            data = requests.data.map { requestToDto(it, user) },
            error = requests.error
        )
    }

    fun requestToDetailedDto(request: Request): RequestDetailedDto {
        return RequestDetailedDto(
            id = request.id,
            start = request.start,
            end = request.end,
            createdFor = request.createdFor?.getAnonName() ?: request.createdBy.getAnonName(),
            districts = request.districts.map(this::districtToDto),
            isOwn = false,
            createdBy = accountToDto(request.createdBy),
            reservedBy = request.reservedBy?.let { accountToDto(it) },
            confirmedAt = request.confirmedAt
        )
    }

    fun paginatedRequestsToDetailedDto(requests: PaginationObject<Request>): RequestDetailedPaginatedDto {
        return RequestDetailedPaginatedDto(
            draw = requests.draw.toLong(),
            recordsTotal = requests.recordsTotal,
            recordsFiltered = requests.recordsFiltered,
            data = requests.data.map { requestToDetailedDto(it) },
            error = requests.error
        )
    }

    fun districtToDto(result: District): DistrictDto {
        return DistrictDto(
            id = result.id,
            name = result.name
        )
    }

    fun offerToDto(offer: Offer, user: LegacyUser?): OfferDto {
        return OfferDto(
            id = offer.id,
            start = offer.start,
            end = offer.end,
            createdFor = offer.createdFor?.getAnonName() ?: offer.createdBy.getAnonName(),
            districts = offer.districts.map(this::districtToDto),
            isOwn = offer.createdBy.legacyUserId == user?.id
        )
    }

    fun paginatedOffersToDto(offers: PaginationObject<Offer>, user: LegacyUser?): OfferPaginatedDto {
        return OfferPaginatedDto(
            draw = offers.draw.toLong(),
            recordsTotal = offers.recordsTotal,
            recordsFiltered = offers.recordsFiltered,
            data = offers.data.map { offerToDto(it, user) },
            error = offers.error
        )
    }

    fun accountToDto(account: Account): AccountDto {
        return AccountDto(
            givenName = account.givenName,
            surname = account.surname,
            legacyUserId = account.legacyUserId,
            email = account.email,
            phone = account.phone
        )
    }

    fun offerToDetailedDto(offer: Offer): OfferDetailedDto {
        return OfferDetailedDto(
            id = offer.id,
            start = offer.start,
            end = offer.end,
            createdFor = offer.createdFor?.getAnonName() ?: offer.createdBy.getAnonName(),
            districts = offer.districts.map(this::districtToDto),
            isOwn = false,
            createdBy = accountToDto(offer.createdBy),
            reservedBy = offer.reservedBy?.let { accountToDto(it) },
            confirmedAt = offer.confirmedAt
        )
    }

    fun paginatedOffersToDetailedDto(offers: PaginationObject<Offer>): OfferDetailedPaginatedDto {
        return OfferDetailedPaginatedDto(
            draw = offers.draw.toLong(),
            recordsTotal = offers.recordsTotal,
            recordsFiltered = offers.recordsFiltered,
            data = offers.data.map { offerToDetailedDto(it) },
            error = offers.error
        )
    }

    fun pilotPaginatedToDto(pilots: PaginationObject<PilotDetails>): PilotPaginatedDto {
        return PilotPaginatedDto(
            draw = pilots.draw.toLong(),
            recordsTotal = pilots.recordsTotal,
            recordsFiltered = pilots.recordsFiltered,
            data = pilots.data.map { pilotToDto(it) },
            error = pilots.error
        )
    }

    private fun pilotToDto(pilot: PilotDetails): PilotDto {
        return PilotDto(
            id = pilot.id,
            givenName = pilot.givenName,
            surname = pilot.surname,
            email = pilot.email,
            phone = pilot.phone
        )
    }

    fun accountPaginatedToDto(accounts: PaginationObject<Account>): AccountPaginatedDto {
        return AccountPaginatedDto(
            draw = accounts.draw.toLong(),
            recordsTotal = accounts.recordsTotal,
            recordsFiltered = accounts.recordsFiltered,
            data = accounts.data.map { accountToDto(it) },
            error = accounts.error
        )

    }
}