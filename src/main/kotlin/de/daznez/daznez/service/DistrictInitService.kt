package de.daznez.daznez.service

import de.daznez.daznez.entity.District
import de.daznez.daznez.external.legacy.LegacyBackendService
import de.daznez.daznez.repository.DistrictRepository
import org.springframework.context.ApplicationListener
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class DistrictInitService(
    private val districtRepository: DistrictRepository,
    private val legacyBackendService: LegacyBackendService
): ApplicationListener<ContextRefreshedEvent> {

    @Transactional
    override fun onApplicationEvent(event: ContextRefreshedEvent) {
        val legacyDistricts = legacyBackendService.readDistricts()

        for (legacyDistrict in legacyDistricts) {
            if (districtRepository.findByName(legacyDistrict.name).isEmpty) {
                districtRepository.save(District(
                    name = legacyDistrict.name
                ))
            }
        }
    }
}