package de.daznez.daznez.service

import de.daznez.daznez.repository.OfferRepository
import de.daznez.daznez.repository.RequestRepository
import de.daznez.dto.TimeframeDto
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.sql.Timestamp
import java.time.Instant

@Service
class BookedOutService(
    private val offerRepository: OfferRepository,
    private val requestRepository: RequestRepository,
    @Value("\${rickshaw.count}")
    private val rickshawCount: Long
) {

    fun bookedOutWithinTimeframes(start: Instant, end: Instant): Boolean {
        return bookedOutTimeframesWithin(start, end).isNotEmpty()
    }

    fun bookedOutTimeframesWithin(start: Instant, end: Instant): List<TimeframeDto> {
        val offers = offerRepository.conflictingReserved(Timestamp.from(start), Timestamp.from(end))
        val requests = requestRepository.conflictingReserved(Timestamp.from(start), Timestamp.from(end))

        val timeframes = offers.map { TimeframeDto(it.start, it.end) } + requests.map { TimeframeDto(it.start, it.end) }
        val bookingStarts = timeframes.map { BookingEvent(BookingEventType.START, it.start!!.toInstant()) }
        val bookingEnds = timeframes.map { BookingEvent(BookingEventType.END, it.end!!.toInstant()) }
        val events = bookingStarts + bookingEnds

        var availableRickshaws = rickshawCount
        val result = mutableListOf<TimeframeDto>()
        var currentBookedOutStart: Instant? = null

        for (event in events.sortedBy { it.time }) {
            when (event.type) {
                BookingEventType.START -> {
                    availableRickshaws -= 1
                    if (availableRickshaws == 0L) {
                        currentBookedOutStart = event.time
                    }
                }
                BookingEventType.END -> {
                    availableRickshaws += 1
                    if (availableRickshaws == 1L) {
                        result += TimeframeDto(Timestamp.from(currentBookedOutStart), Timestamp.from(event.time))
                    }
                }
            }
        }
        return result
    }

    class BookingEvent(
        val type: BookingEventType,
        val time: Instant
    )
    enum class BookingEventType { START, END }
}