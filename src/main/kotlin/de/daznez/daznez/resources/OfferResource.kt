package de.daznez.daznez.resources

import de.daznez.api.OffersApi
import de.daznez.daznez.service.DtoMapper
import de.daznez.daznez.entity.Offer
import de.daznez.daznez.entity.Person
import de.daznez.daznez.external.mail.MailService
import de.daznez.daznez.external.mail.TemplateHandler
import de.daznez.daznez.pagination.Paginator
import de.daznez.daznez.repository.*
import de.daznez.daznez.security.LegacyBackendAuthentication
import de.daznez.daznez.service.AccountService
import de.daznez.daznez.service.BookedOutService
import de.daznez.dto.OfferDetailedPaginatedDto
import de.daznez.dto.OfferDto
import de.daznez.dto.OfferPaginatedDto
import de.daznez.dto.OfferPayloadDto
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.domain.Sort
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException
import java.sql.Timestamp
import java.time.Instant
import java.util.*
import javax.transaction.Transactional

@RestController
class OfferResource(
    private val offerRepository: OfferRepository,
    private val requestRepository: RequestRepository,
    private val pilotRepository: PilotRepository,
    private val personRepository: PersonRepository,
    private val districtRepository: DistrictRepository,
    private val dtoMapper: DtoMapper,
    private val paginator: Paginator,
    private val accountService: AccountService,
    private val mailService: MailService,
    private val templateHandler: TemplateHandler,
    @Value("\${rickshaw.count}")
    private val rickshawCount: Long,
    private val bookedOutService: BookedOutService
): OffersApi {

    @Secured("ROLE_AUTHENTICATED")
    override fun createOffer(offerPayloadDto: OfferPayloadDto): ResponseEntity<OfferDto> {

        val authentication = LegacyBackendAuthentication.require()
        val account = accountService.findForLegacyUser(authentication.legacyUser)

        val districts = districtRepository.findAllById(offerPayloadDto.districtIds)

        if (districts.size != offerPayloadDto.districtIds.size) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND, "Could not find all districts")
        }

        val pilotEmail = offerPayloadDto.onBehalfOf?.email ?: account.email
        if (!pilotRepository.findByEmail(pilotEmail).isPresent) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "MISSING_LICENSE: You do not have a license to ride rickshaws")
        }

        val createdFor = offerPayloadDto.onBehalfOf?.let {
            personRepository.save(
                Person(
                givenName = it.givenName,
                surname = it.surname,
                hint = it.hint,
                phone = it.phone,
                email = it.email
            )
            )
        }

        val offer = offerRepository.save(Offer(
            start = offerPayloadDto.start,
            end = offerPayloadDto.end,
            createdBy = account,
            createdFor = createdFor,
            districts = districts,
            deleteToken = UUID.randomUUID().toString()
        ))
        return ResponseEntity.ok(dtoMapper.offerToDto(offer, authentication.legacyUser))
    }

    override fun readOffersPaginated(
        @RequestParam params: Map<String, String>,
        draw: Long,
        start: Long,
        length: Int,
        searchValue: String?,
        order0Column: Int?,
        order0Dir: String?,
        districtId: Long?
    ): ResponseEntity<OfferPaginatedDto> {
        val authentication = LegacyBackendAuthentication.optional()

        val district = if (districtId != null) {
            districtRepository.findById(districtId).orElseThrow {
                ResponseStatusException(HttpStatus.NOT_FOUND, "Could not find district")
            }
        } else {
            null
        }

        val defaultSort = Sort.by(Sort.Order.desc("start"))
        val result = paginator.paginate(params, defaultSort) {
            offerRepository.search(district, rickshawCount, it)
        }
        return ResponseEntity.ok(dtoMapper.paginatedOffersToDto(result, authentication?.legacyUser))
    }

    @Secured("ROLE_AUTHENTICATED")
    @Transactional
    override fun reserveOffer(id: Long): ResponseEntity<Unit> {
        val authentication = LegacyBackendAuthentication.require()
        val reservedBy = accountService.findForLegacyUser(authentication.legacyUser)

        val offer = offerRepository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND)
        }
        if (bookedOutService.bookedOutWithinTimeframes(offer.start.toInstant(), offer.end.toInstant())) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "NO_RICKSHAW_AVAILABLE: No rickshaw available during the specified time frame")
        }
        offer.reservedBy = reservedBy
        offerRepository.save(offer)
        mailService.sendOfferReservedMail(offer, reservedBy)

        return ResponseEntity.ok(null)
    }

    override fun deleteOffer(id: Long, deleteToken: String): ResponseEntity<String> {
        val offer = offerRepository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND)
        }
        if (offer.deleteToken != deleteToken) {
            throw ResponseStatusException(HttpStatus.UNAUTHORIZED)
        }
        mailService.sendOfferConfirmedMail(offer)
        offer.confirmedAt = Timestamp.from(Instant.now())
        offerRepository.save(offer)
        return ResponseEntity.ok(templateHandler.createOfferDeletedPage(offer))
    }

    @Secured("ROLE_AUTHENTICATED")
    override fun cancelOffer(id: Long): ResponseEntity<Unit> {
        val authentication = LegacyBackendAuthentication.require()

        val offer = offerRepository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND)
        }
        if (!authentication.isAdmin && offer.createdBy.legacyUserId != authentication.legacyUser.id) {
            throw ResponseStatusException(HttpStatus.FORBIDDEN)
        }
        offerRepository.delete(offer)
        return ResponseEntity.ok(null)
    }

    @Secured("ROLE_ADMIN")
    override fun readBookedOffers(
        @RequestParam params: Map<String, String>,
        draw: Long,
        start: Long,
        length: Int,
        searchValue: String?,
        order0Column: Int?,
        order0Dir: String?
    ): ResponseEntity<OfferDetailedPaginatedDto> {
        val defaultSort = Sort.by(Sort.Order.desc("start"))

        val result = paginator.paginate(params, defaultSort) {
            if (searchValue.isNullOrBlank()) {
                offerRepository.findAllBooked(it)
            } else {
                offerRepository.searchAllBooked(searchValue, it)
            }
        }

        return ResponseEntity.ok(dtoMapper.paginatedOffersToDetailedDto(result))
    }
}