package de.daznez.daznez.resources

import de.daznez.api.DistrictsApi
import de.daznez.daznez.service.DtoMapper
import de.daznez.daznez.repository.DistrictRepository
import de.daznez.dto.DistrictDto
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RestController

@RestController
class DistrictResource(
    private val districtRepository: DistrictRepository,
    private val dtoMapper: DtoMapper
): DistrictsApi {

    override fun readDistricts(): ResponseEntity<List<DistrictDto>> {
        val result = districtRepository.findAll()
        return ResponseEntity.ok(result.map(dtoMapper::districtToDto))
    }
}