package de.daznez.daznez.resources

import de.daznez.api.AccountsApi
import de.daznez.daznez.pagination.Paginator
import de.daznez.daznez.repository.AccountRepository
import de.daznez.daznez.security.LegacyBackendAuthentication
import de.daznez.daznez.service.DtoMapper
import de.daznez.dto.AccountPaginatedDto
import org.springframework.data.domain.Sort
import org.springframework.http.ResponseEntity
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class AccountResource(
    private val paginator: Paginator,
    private val accountRepository: AccountRepository,
    private val dtoMapper: DtoMapper
): AccountsApi {

    @Secured("ROLE_AUTHENTICATED")
    override fun amIAdmin(): ResponseEntity<Boolean> {
        val authentication = LegacyBackendAuthentication.require()
        return ResponseEntity.ok(authentication.isAdmin)
    }

    @Secured("ROLE_ADMIN")
    override fun getAccounts(
        @RequestParam params: Map<String, String>,
        draw: Long,
        start: Long,
        length: Int,
        searchValue: String?,
        order0Column: Int?,
        order0Dir: String?
    ): ResponseEntity<AccountPaginatedDto> {
        val defaultSort = Sort.by(Sort.Order.asc("email"))
        val accounts = paginator.paginate(params, defaultSort) {
            if (searchValue.isNullOrBlank()) {
                accountRepository.findAll(it)
            } else {
                accountRepository.search(searchValue, it)
            }
        }
        return ResponseEntity.ok(dtoMapper.accountPaginatedToDto(accounts))
    }
}