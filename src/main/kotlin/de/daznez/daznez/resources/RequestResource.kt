package de.daznez.daznez.resources

import de.daznez.api.RequestsApi
import de.daznez.daznez.service.DtoMapper
import de.daznez.daznez.entity.Person
import de.daznez.daznez.entity.Request
import de.daznez.daznez.external.mail.MailService
import de.daznez.daznez.external.mail.TemplateHandler
import de.daznez.daznez.pagination.Paginator
import de.daznez.daznez.repository.*
import de.daznez.daznez.security.LegacyBackendAuthentication
import de.daznez.daznez.service.AccountService
import de.daznez.daznez.service.BookedOutService
import de.daznez.dto.*
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.domain.Sort
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException
import java.sql.Timestamp
import java.time.Instant
import java.util.*
import javax.transaction.Transactional

@RestController
class RequestResource(
    private val requestRepository: RequestRepository,
    private val offerRepository: OfferRepository,
    private val pilotRepository: PilotRepository,
    private val districtRepository: DistrictRepository,
    private val personRepository: PersonRepository,
    private val dtoMapper: DtoMapper,
    private val paginator: Paginator,
    private val accountService: AccountService,
    private val mailService: MailService,
    private val templateHandler: TemplateHandler,
    @Value("\${rickshaw.count}")
    private val rickshawCount: Long,
    private val bookedOutService: BookedOutService
): RequestsApi {

    @Secured("ROLE_AUTHENTICATED")
    @Transactional
    override fun createRequest(requestPayloadDto: RequestPayloadDto): ResponseEntity<RequestDto> {

        val districts = districtRepository.findAllById(requestPayloadDto.districtIds)

        val authentication = LegacyBackendAuthentication.require()
        val account = accountService.findForLegacyUser(authentication.legacyUser)

        if (districts.size != requestPayloadDto.districtIds.size) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND, "Could not find all districts")
        }

        val createdFor = requestPayloadDto.onBehalfOf?.let {
            personRepository.save(Person(
                givenName = it.givenName,
                surname = it.surname,
                hint = it.hint,
                phone = it.phone,
                email = it.email
            ))
        }

        val request = requestRepository.save(Request(
            start = requestPayloadDto.start,
            end = requestPayloadDto.end,
            districts = districts,
            createdBy = account,
            createdFor = createdFor,
            deleteToken = UUID.randomUUID().toString()
        ))
        return ResponseEntity.ok(dtoMapper.requestToDto(request, authentication.legacyUser))
    }

    override fun readRequestsPaginated(
        @RequestParam params: Map<String, String>,
        draw: Long,
        start: Long,
        length: Int,
        searchValue: String?,
        order0Column: Int?,
        order0Dir: String?,
        districtId: Long?
    ): ResponseEntity<RequestPaginatedDto> {
        val authentication = LegacyBackendAuthentication.optional()

        val district = districtId?.let {
            districtRepository.findById(it).orElseThrow {
                ResponseStatusException(HttpStatus.NOT_FOUND, "Could not find district")
            }
        }

        val defaultSort = Sort.by(Sort.Order.desc("start"))
        val result = paginator.paginate(params, defaultSort) {
            requestRepository.search(district, rickshawCount, it)
        }
        return ResponseEntity.ok(dtoMapper.paginatedRequestsToDto(result, authentication?.legacyUser))
    }

    @Secured("ROLE_AUTHENTICATED")
    override fun reserveRequest(id: Long): ResponseEntity<Unit> {
        val authentication = LegacyBackendAuthentication.require()
        val reservedBy = accountService.findForLegacyUser(authentication.legacyUser)

        if (!pilotRepository.findByEmail(reservedBy.email).isPresent) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "MISSING_LICENSE: You do not have a license to ride rickshaws")
        }

        val request = requestRepository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND)
        }
        if (bookedOutService.bookedOutWithinTimeframes(request.start.toInstant(), request.end.toInstant())) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "NO_RICKSHAW_AVAILABLE: No rickshaw available during the specified time frame")
        }
        request.reservedBy = reservedBy
        requestRepository.save(request)
        mailService.sendRequestReservedMail(request, reservedBy)

        return ResponseEntity.ok(null)
    }

    override fun deleteRequest(id: Long, deleteToken: String): ResponseEntity<String> {
        val request = requestRepository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND)
        }
        if (request.deleteToken != deleteToken) {
            throw ResponseStatusException(HttpStatus.UNAUTHORIZED)
        }
        mailService.sendRequestConfirmedMail(request)
        request.confirmedAt = Timestamp.from(Instant.now())
        requestRepository.save(request)
        return ResponseEntity.ok(templateHandler.createRequestDeletedPage(request))
    }

    @Secured("ROLE_AUTHENTICATED")
    override fun cancelRequest(id: Long): ResponseEntity<Unit> {
        val authentication = LegacyBackendAuthentication.require()

        val request = requestRepository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND)
        }
        if (!authentication.isAdmin && request.createdBy.legacyUserId != authentication.legacyUser.id) {
            throw ResponseStatusException(HttpStatus.FORBIDDEN)
        }
        requestRepository.delete(request)
        return ResponseEntity.ok(null)
    }

    @Secured("ROLE_ADMIN")
    override fun readBookedRequests(
        @RequestParam params: Map<String, String>,
        draw: Long,
        start: Long,
        length: Int,
        searchValue: String?,
        order0Column: Int?,
        order0Dir: String?
    ): ResponseEntity<RequestDetailedPaginatedDto> {
        val defaultSort = Sort.by(Sort.Order.desc("start"))

        val result = paginator.paginate(params, defaultSort) {
            if (searchValue.isNullOrBlank()) {
                requestRepository.findAllBooked(it)
            } else {
                requestRepository.searchAllBooked(searchValue, it)
            }
        }

        return ResponseEntity.ok(dtoMapper.paginatedRequestsToDetailedDto(result))
    }
}