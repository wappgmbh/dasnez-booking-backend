package de.daznez.daznez.resources

import de.daznez.api.PilotsApi
import de.daznez.daznez.entity.Pilot
import de.daznez.daznez.model.PilotDetails
import de.daznez.daznez.pagination.Paginator
import de.daznez.daznez.repository.AccountRepository
import de.daznez.daznez.repository.PilotRepository
import de.daznez.daznez.service.DtoMapper
import de.daznez.dto.PilotPaginatedDto
import de.daznez.dto.PilotPayloadDto
import org.springframework.data.domain.Sort
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.server.ResponseStatusException

@Controller
class PilotResource(
    private val pilotRepository: PilotRepository,
    private val accountRepository: AccountRepository,
    private val paginator: Paginator,
    private val dtoMapper: DtoMapper
): PilotsApi {

    @Secured("ROLE_ADMIN")
    override fun addPilot(pilotPayloadDto: PilotPayloadDto): ResponseEntity<Unit> {
        if (pilotRepository.findByEmail(pilotPayloadDto.email).isPresent) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "DUPLICATE_EMAIL: This pilot is already registered")
        }
        pilotRepository.save(Pilot(
            email = pilotPayloadDto.email
        ))
        return ResponseEntity.ok(null)
    }

    @Secured("ROLE_ADMIN")
    override fun getPilotsPaginated(
        @RequestParam params: Map<String, String>,
        draw: Long,
        start: Long,
        length: Int,
        searchValue: String?,
        order0Column: Int?,
        order0Dir: String?
    ): ResponseEntity<PilotPaginatedDto> {
        val defaultSort = Sort.by(Sort.Order.asc("email"))
        val result = paginator.paginate(params, defaultSort) {
            if (searchValue.isNullOrBlank()) {
                pilotRepository.findAll(it)
            } else {
                pilotRepository.search(searchValue, it)
            }.map { pilot ->
                val account = accountRepository.findByEmail(pilot.email).orElse(null)
                if (account != null) {
                    PilotDetails(
                        id = pilot.id,
                        givenName = account.givenName,
                        surname = account.surname,
                        email = pilot.email,
                        phone = account.phone
                    )
                } else {
                    PilotDetails(
                        id = pilot.id,
                        email = pilot.email
                    )
                }
            }
        }

        return ResponseEntity.ok(dtoMapper.pilotPaginatedToDto(result))
    }

    override fun deletePilot(id: Long): ResponseEntity<Unit> {
        val pilot = pilotRepository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND)
        }
        pilotRepository.delete(pilot)
        return ResponseEntity.ok(null)
    }
}