package de.daznez.daznez.resources

import de.daznez.api.RickshawsApi
import de.daznez.daznez.service.BookedOutService
import de.daznez.dto.TimeframeDto
import org.springframework.http.ResponseEntity
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Controller
import java.time.Instant
import java.time.ZoneId

@Controller
class RickshawResource(
    private val bookedOutService: BookedOutService
): RickshawsApi {

    private val timezone = ZoneId.of("Europe/Berlin")

    @Secured("ROLE_AUTHENTICATED")
    override fun getRickshawAvailabilityDuring(start: Long, end: Long): ResponseEntity<Boolean> {
        val startInstant = Instant.ofEpochSecond(start)
        val endInstant = Instant.ofEpochSecond(end)
        return ResponseEntity.ok(!bookedOutService.bookedOutWithinTimeframes(startInstant, endInstant))
    }

    @Secured("ROLE_AUTHENTICATED")
    override fun getBookingsDuringDay(day: Long): ResponseEntity<List<TimeframeDto>> {
        val timeInDay = Instant.ofEpochSecond(day).atZone(timezone)
        val start = timeInDay.withHour(0).withMinute(0).withSecond(0).withNano(0)
        val end = timeInDay.plusDays(1)
        val result = bookedOutService.bookedOutTimeframesWithin(start.toInstant(), end.toInstant())
        return ResponseEntity.ok(result)
    }
}
