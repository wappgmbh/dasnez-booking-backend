package de.daznez.daznez.entity

import javax.persistence.*

@Entity
@Table(name = "district")
data class District(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = -1,

    @Column(nullable = false)
    var name: String
)