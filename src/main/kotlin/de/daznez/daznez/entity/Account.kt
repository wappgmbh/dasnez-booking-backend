package de.daznez.daznez.entity

import javax.persistence.*

/**
 * Someone that has an account in the legacy backend
 */
@Entity
@Table(name = "account")
data class Account(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = -1,

    @Column(nullable = false)
    var givenName: String,

    @Column(nullable = false)
    var surname: String,

    @Column(nullable = false)
    var legacyUserId: String,

    @Column(nullable = false)
    var email: String,

    @Column(nullable = false)
    var phone: String
) {

    fun getAnonName() = "$givenName ${surname.first()}"
}
