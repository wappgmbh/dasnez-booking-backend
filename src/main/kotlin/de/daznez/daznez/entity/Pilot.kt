package de.daznez.daznez.entity

import javax.persistence.*

@Entity
class Pilot(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = -1,

    @Column(nullable = false)
    var email: String
)