package de.daznez.daznez.entity

import javax.persistence.*

/**
 * Someone for whom a request is made
 */
@Entity
@Table(name = "person")
data class Person(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = -1,

    @Column(nullable = false)
    var givenName: String,

    @Column(nullable = false)
    var surname: String,

    @Column(nullable = false)
    var hint: String,

    @Column(nullable = false)
    var phone: String,

    @Column(nullable = false)
    var email: String
) {

    fun getAnonName() = "$givenName ${surname.first()}"
}