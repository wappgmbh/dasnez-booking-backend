package de.daznez.daznez.entity

import java.sql.Timestamp
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import javax.persistence.*

@Entity
@Table(name = "offer")
data class Offer(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    override var id: Long = -1,

    @Column(nullable = false)
    override var start: Timestamp,

    /**
     * Must be on the same day as start
     */
    @Column(name = "\"end\"", nullable = false)
    override var end: Timestamp,

    @JoinTable(
        name = "offer_has_district",
        joinColumns = [JoinColumn(name = "offer_id")],
        inverseJoinColumns = [JoinColumn(name = "district_id")]
    )
    @ManyToMany
    override var districts: List<District> = mutableListOf(),

    @ManyToOne(optional = false)
    override var createdBy: Account,

    @ManyToOne(optional = true)
    override var createdFor: Person? = null,

    @Column(name = "deleteToken", nullable = false)
    override var deleteToken: String,

    @ManyToOne(optional = true)
    override var reservedBy: Account? = null,

    @Column(nullable = true)
    override var confirmedAt: Timestamp? = null
): Ad {

    val createdForGivenName @Transient get() = createdFor?.givenName ?: createdBy.givenName

    val createdForSurname @Transient get() = createdFor?.surname ?: createdBy.surname

    val createdForPhone @Transient get() = createdFor?.phone ?: createdBy.phone

    val createdForEmail @Transient get() = createdFor?.email ?: createdBy.email

    val districtsStr @Transient get() = districts.joinToString(", ") { it.name }

    val date @Transient get() = ZonedDateTime.ofInstant(start.toInstant(), TIME_ZONE).format(dateFormatter)

    val startTime @Transient get() = ZonedDateTime.ofInstant(start.toInstant(), TIME_ZONE).format(timeFormatter)

    val endTime @Transient get() = ZonedDateTime.ofInstant(end.toInstant(), TIME_ZONE).format(timeFormatter)

    companion object {

        private val TIME_ZONE = ZoneId.of("Europe/Berlin")

        private val dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy")

        private val timeFormatter = DateTimeFormatter.ofPattern("HH:mm")
    }
}
