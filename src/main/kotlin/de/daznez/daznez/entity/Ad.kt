package de.daznez.daznez.entity

import java.sql.Timestamp

interface Ad {

    var id: Long

    var start: Timestamp

    var end: Timestamp

    var districts: List<District>

    var createdBy: Account

    var createdFor: Person?

    var deleteToken: String

    var reservedBy: Account?

    var confirmedAt: Timestamp?
}