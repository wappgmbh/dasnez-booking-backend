package de.daznez.daznez.repository

import de.daznez.daznez.entity.Account
import de.daznez.daznez.entity.District
import de.daznez.daznez.entity.Request
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import java.sql.Timestamp

interface RequestRepository: JpaRepository<Request, Long> {

    @Query("SELECT DISTINCT request " +
            "FROM Request request " +
            "LEFT JOIN request.districts district " +
            "WHERE (:district IS NULL OR " +
            "district = :district) " +
            "AND request.reservedBy IS NULL " +
            "AND ((SELECT COUNT(co) " +
            "FROM Offer co " +
            "WHERE co.end >= request.start " +
            "AND co.start < request.end " +
            "AND co.reservedBy IS NOT NULL) " +
            "+ (SELECT COUNT(cr) " +
            "FROM Request cr " +
            "WHERE cr.end >= request.start " +
            "AND cr.start < request.end " +
            "AND cr.reservedBy IS NOT NULL)) " +
            "< :rickshawCount")
    fun search(district: District?, rickshawCount: Long, pageable: Pageable): Page<Request>

    @Query("SELECT request " +
            "FROM Request request " +
            "WHERE request.start > :startTime " +
            "AND request.createdBy = :createdBy " +
            "AND request.confirmedAt IS NOT NULL")
    fun confirmedRequestsCreatedByAndAfter(createdBy: Account, startTime: Timestamp): List<Request>

    @Query("SELECT request " +
            "FROM Request request " +
            "WHERE request.start > :startTime " +
            "AND request.reservedBy = :reservedBy " +
            "AND request.confirmedAt IS NOT NULL")
    fun confirmedRequestsReservedByAndAfter(reservedBy: Account, startTime: Timestamp): List<Request>

    @Query("SELECT request " +
            "FROM Request request " +
            "WHERE request.confirmedAt IS NOT NULL")
    fun findAllBooked(pageable: Pageable): Page<Request>

    @Query("SELECT request " +
            "FROM Request request " +
            "JOIN request.createdBy createdBy " +
            "LEFT JOIN request.createdFor createdFor " +
            "LEFT JOIN request.reservedBy reservedBy "  +
            "WHERE request.confirmedAt IS NOT NULL " +
            "AND (LOWER(createdBy.givenName) LIKE LOWER(CONCAT('%', :search, '%')) " +
            "OR LOWER(createdBy.surname) LIKE LOWER(CONCAT('%', :search, '%')) " +
            "OR LOWER(createdFor.givenName) LIKE LOWER(CONCAT('%', :search, '%')) " +
            "OR LOWER(createdFor.surname) LIKE LOWER(CONCAT('%', :search, '%')) " +
            "OR LOWER(reservedBy.givenName) LIKE LOWER(CONCAT('%', :search, '%')) " +
            "OR LOWER(reservedBy.surname) LIKE LOWER(CONCAT('%', :search, '%')))")
    fun searchAllBooked(search: String, pageable: Pageable): Page<Request>

    @Query("SELECT request " +
            "FROM Request request " +
            "WHERE request.end >= :start " +
            "AND request.start < :end " +
            "AND request.reservedBy IS NOT NULL")
    fun conflictingReserved(start: Timestamp, end: Timestamp): List<Request>
}