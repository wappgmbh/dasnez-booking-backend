package de.daznez.daznez.repository

import de.daznez.daznez.entity.Account
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import java.util.*

interface AccountRepository: JpaRepository<Account, Long> {

    fun findByLegacyUserId(legacyUserId: String): Optional<Account>

    @Query("SELECT account " +
            "FROM Account account " +
            "WHERE LOWER(account.email) LIKE LOWER(CONCAT('%', :search, '%')) " +
            "OR LOWER(account.givenName) LIKE LOWER(CONCAT('%', :search, '%')) " +
            "OR LOWER(account.surname) LIKE LOWER(CONCAT('%', :search, '%'))")
    fun search(search: String, pageable: Pageable): Page<Account>

    fun findByEmail(email: String): Optional<Account>
}