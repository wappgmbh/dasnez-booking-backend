package de.daznez.daznez.repository

import de.daznez.daznez.entity.Pilot
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import java.util.*

interface PilotRepository: JpaRepository<Pilot, Long> {
    @Query("SELECT pilot " +
            "FROM Pilot pilot " +
            "WHERE LOWER(pilot.email) LIKE LOWER(CONCAT('%', :search, '%'))")
    fun search(search: String, pageable: Pageable): Page<Pilot>

    fun findByEmail(email: String): Optional<Pilot>
}