package de.daznez.daznez.repository

import de.daznez.daznez.entity.District
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface DistrictRepository: JpaRepository<District, Long> {

    fun findByName(name: String): Optional<District>
}