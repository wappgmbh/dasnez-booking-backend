package de.daznez.daznez.repository

import de.daznez.daznez.entity.Account
import de.daznez.daznez.entity.District
import de.daznez.daznez.entity.Offer
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import java.sql.Timestamp

interface OfferRepository: JpaRepository<Offer, Long> {

    @Query("SELECT DISTINCT offer " +
            "FROM Offer offer " +
            "LEFT JOIN offer.districts district " +
            "WHERE (:district IS NULL OR " +
            "district = :district) " +
            "AND offer.reservedBy IS NULL " +
            "AND ((SELECT COUNT(co) " +
            "FROM Offer co " +
            "WHERE co.end >= offer.start " +
            "AND co.start < offer.end " +
            "AND co.reservedBy IS NOT NULL) " +
            "+ (SELECT COUNT(cr) " +
            "FROM Request cr " +
            "WHERE cr.end >= offer.start " +
            "AND cr.start < offer.end " +
            "AND cr.reservedBy IS NOT NULL)) " +
            "< :rickshawCount")
    fun search(district: District?, rickshawCount: Long, pageable: Pageable): Page<Offer>

    @Query("SELECT offer " +
            "FROM Offer offer " +
            "WHERE offer.start > :startTime " +
            "AND offer.createdBy = :createdBy " +
            "AND offer.confirmedAt IS NOT NULL ")

    fun confirmedOffersCreatedByAndAfter(createdBy: Account, startTime: Timestamp): List<Offer>

    @Query("SELECT offer " +
            "FROM Offer offer " +
            "WHERE offer.start > :startTime " +
            "AND offer.reservedBy = :reservedBy " +
            "AND offer.confirmedAt IS NOT NULL")
    fun confirmedOffersReservedByAndAfter(reservedBy: Account, startTime: Timestamp): List<Offer>

    @Query("SELECT offer " +
            "FROM Offer offer " +
            "WHERE offer.confirmedAt IS NOT NULL")
    fun findAllBooked(pageable: Pageable): Page<Offer>

    @Query("SELECT offer " +
            "FROM Offer offer " +
            "JOIN offer.createdBy createdBy " +
            "LEFT JOIN offer.createdFor createdFor " +
            "LEFT JOIN offer.reservedBy reservedBy "  +
            "WHERE offer.confirmedAt IS NOT NULL " +
            "AND (LOWER(createdBy.givenName) LIKE LOWER(CONCAT('%', :search, '%')) " +
            "OR LOWER(createdBy.surname) LIKE LOWER(CONCAT('%', :search, '%')) " +
            "OR LOWER(createdFor.givenName) LIKE LOWER(CONCAT('%', :search, '%')) " +
            "OR LOWER(createdFor.surname) LIKE LOWER(CONCAT('%', :search, '%')) " +
            "OR LOWER(reservedBy.givenName) LIKE LOWER(CONCAT('%', :search, '%')) " +
            "OR LOWER(reservedBy.surname) LIKE LOWER(CONCAT('%', :search, '%')))")
    fun searchAllBooked(search: String, pageable: Pageable): Page<Offer>

    @Query("SELECT offer " +
            "FROM Offer offer " +
            "WHERE offer.end >= :start " +
            "AND offer.start < :end " +
            "AND offer.reservedBy IS NOT NULL")
    fun conflictingReserved(start: Timestamp, end: Timestamp): List<Offer>
}