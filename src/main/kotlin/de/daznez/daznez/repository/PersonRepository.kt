package de.daznez.daznez.repository

import de.daznez.daznez.entity.Person
import org.springframework.data.jpa.repository.JpaRepository

interface PersonRepository: JpaRepository<Person, Long>