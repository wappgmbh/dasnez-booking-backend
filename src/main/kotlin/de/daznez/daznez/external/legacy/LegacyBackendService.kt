package de.daznez.daznez.external.legacy

import org.springframework.stereotype.Service

@Service
interface LegacyBackendService {

    fun readUser(userId: String, token: String?): LegacyUser?

    fun readDistricts(): List<LegacyDistrict>
}