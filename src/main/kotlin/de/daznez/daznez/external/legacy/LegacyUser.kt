package de.daznez.daznez.external.legacy

class LegacyUser {
    lateinit var id: String
    lateinit var username: String
    var email: String? = null
    var groups: MutableList<LegacyGroup>? = null
    lateinit var firstname: String
    lateinit var lastname: String
    var city: String? = null
    var district: String? = null
    var street: String? = null
    var zip: String? = null
    var phone: String? = null
    var birthdate: String? = null
}