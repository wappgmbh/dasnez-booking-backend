package de.daznez.daznez.external.legacy

import com.fasterxml.jackson.annotation.JsonProperty

class LegacyGroup {

    @JsonProperty("user_id")
    lateinit var userId: String

    @JsonProperty("group_id")
    lateinit var groupId: String

    @JsonProperty("is_active")
    var isActive: Boolean = false

    @JsonProperty("group_name")
    lateinit var groupName: String
}