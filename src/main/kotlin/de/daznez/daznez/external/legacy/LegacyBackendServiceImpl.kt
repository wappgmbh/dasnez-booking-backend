package de.daznez.daznez.external.legacy

import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.server.ResponseStatusException

@Service
class LegacyBackendServiceImpl(
    restTemplateBuilder: RestTemplateBuilder,
    @Value("\${external.legacy.base-url}")
    private val baseUrl: String,
    @Value("\${external.legacy.municipality.id}")
    private val districtId: String,
    @Value("\${external.legacy.districts.path}")
    private val districtsPath: String
): LegacyBackendService {

    private val restTemplate = restTemplateBuilder.build()

    override fun readUser(userId: String, token: String?): LegacyUser? {
        val headers = HttpHeaders()
        headers["X-Auth-Token"] = token
        val httpEntity = HttpEntity(null, headers)
        val url = "$baseUrl/users/$userId"

        val response = try {
            restTemplate.exchange(url, HttpMethod.GET, httpEntity, LegacyUser::class.java)
        } catch (ex: Exception) {
            throw RuntimeException("Failed to read user $userId using $url from legacy backend", ex)
        }

        return when {
            response.statusCode.is2xxSuccessful -> {
                val body = response.body!!
                return if (body.username == "Gelöschter Benutzer ") {
                    return null
                } else {
                    body
                }
            }
            response.statusCode == HttpStatus.NOT_FOUND -> null
            response.statusCode.is4xxClientError -> throw ResponseStatusException(HttpStatus.UNAUTHORIZED, "Failed retrieve user from legacy backend: ${response.statusCode}")
            else -> throw RuntimeException("Failed to read user $userId from legacy backend: ${response.statusCode}")
        }
    }

    override fun readDistricts(): List<LegacyDistrict> {
        return readDistricts(districtId)
    }

    private fun readDistricts(districtId: String): List<LegacyDistrict> {
        val url = baseUrl + districtsPath.format(districtId)
        val typeRef = object: ParameterizedTypeReference<LegacyPaginated<LegacyDistrict>>() {}
        val response = restTemplate.exchange(url, HttpMethod.GET, HttpEntity(null, null), typeRef)
        when {
            response.statusCode.is2xxSuccessful -> return response.body!!.items
            else -> throw RuntimeException("Failed to read districts from legacy backend: ${response.statusCode}")
        }
    }
}
