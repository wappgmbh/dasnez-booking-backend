package de.daznez.daznez.external.mail

import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.AWSCredentialsProviderChain
import com.amazonaws.auth.EC2ContainerCredentialsProviderWrapper
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.auth.profile.ProfileCredentialsProvider
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class AwsConfiguration(
    @Value("\${aws.profile}")
    private val awsProfile: String
) {

    @Bean
    fun awsCredentials(): AWSCredentialsProvider {
        return AWSCredentialsProviderChain(listOf(
            EnvironmentVariableCredentialsProvider(),
            ProfileCredentialsProvider(awsProfile),
            EC2ContainerCredentialsProviderWrapper()
        ))
    }
}