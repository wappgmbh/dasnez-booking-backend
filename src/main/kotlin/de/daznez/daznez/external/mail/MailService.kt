package de.daznez.daznez.external.mail

import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.regions.Regions
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder
import com.amazonaws.services.simpleemail.model.*
import de.daznez.daznez.entity.Account
import de.daznez.daznez.entity.Ad
import de.daznez.daznez.entity.Offer
import de.daznez.daznez.entity.Request
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class MailService(
    @Value("\${mail.from}")
    private val mailFromAddress: String,
    awsCredentialsProvider: AWSCredentialsProvider,
    private val templateHandler: TemplateHandler,
    @Value("\${mail.delete-offer-link}")
    private val mailDeleteOfferLink: String,
    @Value("\${mail.delete-request-link}")
    private val mailDeleteRequestLink: String
) {

    private val client = AmazonSimpleEmailServiceClientBuilder.standard()
        .withCredentials(awsCredentialsProvider)
        .withRegion(Regions.EU_CENTRAL_1)
        .build()

    fun sendOfferReservedMail(offer: Offer, acceptedBy: Account) {
        val content = templateHandler.createOfferReserved(offer, acceptedBy)
        sendMail(
            to = listOf(offer.createdForEmail),
            subject = "Angebot angenommen",
            title = "${acceptedBy.givenName} ${acceptedBy.surname} hat Ihr Angebot angenommen",
            content = content,
            buttonText = "Bestätigen",
            buttonLink = mailDeleteOfferLink.format(offer.id, offer.deleteToken)
        )
    }

    fun sendOfferConfirmedMail(offer: Offer) {
        val content = templateHandler.createOfferConfirmed(offer)
        sendMail(
            to = listOf(offer.reservedBy!!.email),
            subject = "Anfrage bestätigt",
            title = "${offer.createdForGivenName} ${offer.createdForSurname} hat Ihre Anfrage bestätigt",
            content = content
        )
    }

    fun sendRequestReservedMail(request: Request, reservedBy: Account) {
        val content = templateHandler.createRequestReserved(request, reservedBy)
        sendMail(
            to = listOf(request.createdForEmail),
            subject = "Anfrage angenommen",
            title = "${reservedBy.givenName} ${reservedBy.surname} hat Ihre Anfrage angenommen",
            content = content,
            buttonText = "Bestätigen",
            buttonLink = mailDeleteRequestLink.format(request.id, request.deleteToken)
        )
    }

    fun sendRequestConfirmedMail(request: Request) {
        val content = templateHandler.createRequestConfirmed(request)
        sendMail(
            to = listOf(request.reservedBy!!.email),
            subject = "Angebot bestätigt",
            title = "${request.createdForGivenName} ${request.createdForSurname} hat Ihr Angebot bestätigt",
            content = content
        )
    }

    fun sendAccountDeletedMail(to: String, account: Account, ad: Ad) {
        val content = templateHandler.createAccountDeletedMail(account, ad)
        sendMail(
            to = listOf(to),
            subject = "Geplante Rikschafahrt - Account gelöscht",
            title = "Account von ${account.givenName} ${account.surname} wurde gelöscht",
            content = content
        )
    }

    fun sendMail(
        to: List<String>,
        subject: String,
        title: String,
        content: String,
        buttonText: String? = null,
        buttonLink: String? = null,
        replyTo: String? = null
    ) {
        val htmlBody = templateHandler.createMail(title, content, buttonText, buttonLink)
        sendRawMail(to, subject, htmlBody, replyTo)
    }

    fun sendRawMail(to: List<String>, subject: String, htmlBody: String, replyTo: String?) {

        var request = SendEmailRequest()
            .withDestination(Destination().withToAddresses(to))
            .withMessage(
                Message()
                .withBody(
                    Body()
                    .withHtml(Content().withCharset("UTF-8").withData(htmlBody))
                )
                .withSubject(Content().withCharset("UTF-8").withData(subject)))
            .withSource(mailFromAddress)
        if (replyTo != null) {
            request = request.withReplyToAddresses(replyTo)
        }
        client.sendEmail(request)
    }
}