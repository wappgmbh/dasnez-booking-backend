package de.daznez.daznez.external.mail

import com.samskivert.mustache.Mustache
import de.daznez.daznez.entity.Account
import de.daznez.daznez.entity.Ad
import de.daznez.daznez.entity.Offer
import de.daznez.daznez.entity.Request
import org.springframework.boot.autoconfigure.mustache.MustacheResourceTemplateLoader
import org.springframework.stereotype.Component
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

@Component
class TemplateHandler {

    private val mustache = Mustache.compiler()
        .defaultValue("N/A")
        .withLoader(MustacheResourceTemplateLoader("classpath:/templates/", ".mustache"))

    private val timeZone = ZoneId.of("Europe/Berlin")

    private val mailTemplate = mustache.loadTemplate("mail")
    private val requestReservedTemplate = mustache.loadTemplate("request-reserved")
    private val offerReservedTemplate = mustache.loadTemplate("offer-reserved")
    private val requestConfirmedTemplate = mustache.loadTemplate("request-confirmed")
    private val offerConfirmedTemplate = mustache.loadTemplate("offer-confirmed")
    private val requestDeletedTemplate = mustache.loadTemplate("request-deleted")
    private val offerDeletedTemplate = mustache.loadTemplate("offer-deleted")
    private val accountDeletedTemplate = mustache.loadTemplate("account-deleted")

    private val dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy")

    private val timeFormatter = DateTimeFormatter.ofPattern("HH:mm")

    fun createMail(title: String, content: String, buttonText: String?, buttonLink: String?): String {
        return mailTemplate.execute(MailTemplateParams(title, content, buttonText, buttonLink))
    }

    fun createOfferReserved(offer: Offer, acceptedBy: Account): String {
        val start = ZonedDateTime.ofInstant(offer.start.toInstant(), timeZone)
        val end = ZonedDateTime.ofInstant(offer.end.toInstant(), timeZone)
        val params = ReservedTemplateParams(
            acceptedBy = acceptedBy,
            date = start.format(dateFormatter),
            startTime = start.format(timeFormatter),
            endTime = end.format(timeFormatter)
        )
        return offerReservedTemplate.execute(params)
    }

    fun createRequestReserved(request: Request, acceptedBy: Account): String {
        val start = ZonedDateTime.ofInstant(request.start.toInstant(), timeZone)
        val end = ZonedDateTime.ofInstant(request.end.toInstant(), timeZone)
        val params = ReservedTemplateParams(
            acceptedBy = acceptedBy,
            date = start.format(dateFormatter),
            startTime = start.format(timeFormatter),
            endTime = end.format(timeFormatter)
        )
        return requestReservedTemplate.execute(params)
    }

    fun createRequestConfirmed(request: Request): String {
        return requestConfirmedTemplate.execute(request)
    }

    fun createOfferConfirmed(offer: Offer): String {
        return offerConfirmedTemplate.execute(offer)
    }

    fun createRequestDeletedPage(request: Request): String {
        return requestDeletedTemplate.execute(request)
    }

    fun createOfferDeletedPage(offer: Offer): String {
        return offerDeletedTemplate.execute(offer)
    }

    fun createAccountDeletedMail(account: Account, ad: Ad): String {
        return accountDeletedTemplate.execute(object {
            val account = account
            val ad = ad
        })
    }

    internal class MailTemplateParams(
        val title: String,
        val content: String,
        val buttonText: String?,
        val buttonLink: String?
    )

    internal class ReservedTemplateParams(
        val acceptedBy: Account,
        val date: String,
        val startTime: String,
        val endTime: String
    )
}