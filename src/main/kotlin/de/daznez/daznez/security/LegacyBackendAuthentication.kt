package de.daznez.daznez.security

import de.daznez.daznez.external.legacy.LegacyUser
import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder

class LegacyBackendAuthentication(
    val legacyUser: LegacyUser,
    val isAdmin: Boolean
): Authentication {

    private var authenticated: Boolean = true

    override fun getName(): String {
        return legacyUser.username
    }

    override fun getAuthorities(): MutableCollection<GrantedAuthority> {
        return if (isAdmin) {
            mutableListOf(AUTHENTICATED, ADMIN)
        } else {
            mutableListOf(AUTHENTICATED)
        }
    }

    override fun getCredentials(): Any? {
        return null
    }

    override fun getDetails(): Any? {
        return null
    }

    override fun getPrincipal(): Any {
        return legacyUser.username
    }

    override fun isAuthenticated(): Boolean {
        return authenticated
    }

    override fun setAuthenticated(isAuthenticated: Boolean) {
        this.authenticated = isAuthenticated
    }

    companion object {
        val AUTHENTICATED = SimpleGrantedAuthority("ROLE_AUTHENTICATED")

        val ADMIN = SimpleGrantedAuthority("ROLE_ADMIN")

        fun require(): LegacyBackendAuthentication {
            return SecurityContextHolder.getContext().authentication as LegacyBackendAuthentication
        }

        fun optional(): LegacyBackendAuthentication? {
            return SecurityContextHolder.getContext().authentication as? LegacyBackendAuthentication
        }
    }
}