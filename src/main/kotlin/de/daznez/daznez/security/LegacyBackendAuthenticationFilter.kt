package de.daznez.daznez.security

import de.daznez.daznez.external.legacy.LegacyBackendService
import de.daznez.daznez.external.legacy.LegacyUser
import de.daznez.daznez.service.AccountService
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import org.springframework.web.server.ResponseStatusException
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class LegacyBackendAuthenticationFilter(
    @Value("\${security.auth.header.user-id}")
    private val userIdHeaderName: String,
    @Value("\${security.auth.header.token}")
    private val tokenHeaderName: String,
    @Value("\${external.legacy.regional-admin-group-id}")
    private val regionalAdminGroupId: String,
    @Value("\${external.legacy.global-admin-group-id}")
    private val globalAdminGroupId: String,
    private val legacyBackendService: LegacyBackendService,
    private val accountService: AccountService
): OncePerRequestFilter() {

    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain
    ) {
        val userId = request.getHeader(userIdHeaderName)
        val token = request.getHeader(tokenHeaderName)

        if (userId != null && token != null) {
            val legacyUser = try {
                legacyBackendService.readUser(userId, token)
            } catch (ex: ResponseStatusException) {
                response.sendError(ex.rawStatusCode, ex.reason)
                return
            } catch (ex: Exception) {
                response.sendError(500, "Failed to authenticate with legacy backend")
                ex.printStackTrace()
                return
            }
            if (legacyUser == null) {
                return response.sendError(401, "Invalid user id or token")
            }
            if (legacyUser.email != null) {
                accountService.findForLegacyUser(legacyUser)
                SecurityContextHolder.getContext().authentication = LegacyBackendAuthentication(legacyUser, isAdmin(legacyUser))
            } else {
                response.sendError(401, "Invalid user id or token")
                return
            }
        }

        filterChain.doFilter(request, response)
    }

    private fun isAdmin(legacyUser: LegacyUser): Boolean {
        return legacyUser.groups!!.any {
            it.isActive && (it.groupId == regionalAdminGroupId || it.groupId == globalAdminGroupId)
        }
    }
}