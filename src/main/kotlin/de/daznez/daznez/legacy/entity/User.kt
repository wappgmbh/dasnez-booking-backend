package de.daznez.daznez.legacy.entity

import java.util.*
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "users")
class User {

    @Id
    var id: UUID? = null

    var email: String? = null

    var phone: String? = null
}
