package de.daznez.daznez.legacy.repository

import de.daznez.daznez.legacy.entity.User
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface UserRepository: JpaRepository<User, UUID>
