package de.daznez.daznez.model

class PilotDetails(
    val id: Long,
    val givenName: String? = null,
    val surname: String? = null,
    val email: String,
    val phone: String? = null
)