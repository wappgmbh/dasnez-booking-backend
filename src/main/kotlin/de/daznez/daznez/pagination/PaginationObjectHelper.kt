package de.daznez.daznez.pagination

import org.springframework.data.domain.Page

object PaginationObjectHelper {

    fun <T> buildPaginationResponseObject(
        paginationRequest: PaginationRequestObject,
        page: Page<T>
    ): PaginationObject<T> {
        val totalCount = page.totalElements
        val filteredCount = page.totalElements

        // Building response object
        return PaginationObject(page.content, paginationRequest.draw, filteredCount, totalCount, null)
    }
}