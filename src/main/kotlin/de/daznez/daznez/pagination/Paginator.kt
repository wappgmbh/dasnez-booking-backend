package de.daznez.daznez.pagination

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Component

@Component
class Paginator {

    fun <T> paginate(
        queryParams: Map<String, String>,
        defaultSort: Sort,
        getItems: (Pageable) -> Page<T>
    ): PaginationObject<T> {
        val paginationRequestObject = PaginationRequestObject.fromQueryParameters(queryParams, defaultSort)
        val items = getItems(paginationRequestObject)
        return PaginationObjectHelper.buildPaginationResponseObject(paginationRequestObject, items)
    }
}