package de.daznez.daznez.pagination

import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException
import java.lang.Exception
import java.util.ArrayList

class PaginationRequestObject private constructor(
    val draw: Int,
    val start: Int,
    val length: Int,
    val searchValue: String?,
    private val sort: Sort
) : Pageable {

    override fun getPageNumber(): Int {
        return start / length
    }

    override fun getPageSize(): Int {
        return length
    }

    override fun getOffset(): Long {
        return start.toLong()
    }

    override fun getSort(): Sort {
        return sort
    }

    override fun next(): Pageable {
        return PageRequest.of(this.pageNumber + 1, this.pageSize)
    }

    override fun previousOrFirst(): Pageable {
        return PageRequest.of(Integer.max(this.pageNumber - 1, 0), this.pageSize)
    }

    override fun first(): Pageable {
        return PageRequest.of(0, this.pageSize)
    }

    override fun hasPrevious(): Boolean {
        return false
    }

    override fun withPage(i: Int): Pageable {
        return PaginationRequestObject(draw, i * length, length, searchValue, sort)
    }

    companion object {

        private fun assertInt(value: String?, fallback: Int): Int {
            return if (value == null) {
                fallback
            } else try {
                value.toInt()
            } catch (e: Exception) {
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "could not parse parameter")
            }
        }

        fun fromQueryParameters(queryParameters: Map<String, String>, defaultSort: Sort): PaginationRequestObject {
            val draw = assertInt(queryParameters["draw"], 0)
            val start = assertInt(queryParameters["start"], 0)
            val length = assertInt(queryParameters["length"], -1)
            val searchValue = queryParameters["search[value]"]
            val orders: MutableList<Sort.Order> = ArrayList()
            val order0Column = assertInt(queryParameters["order[0][column]"], -1)
            if (order0Column != -1) {
                val order0Dir = queryParameters["order[0][dir]"]
                val order0Field = queryParameters["columns[$order0Column][data]"]
                val order0DirEnum: Sort.Direction = try {
                    if (order0Dir == null) {
                        Sort.Direction.ASC
                    } else {
                        Sort.Direction.fromString(order0Dir)
                    }
                } catch (e: Exception) {
                    throw ResponseStatusException(HttpStatus.BAD_REQUEST, "illegal order definition")
                }
                if (order0Field != null) {
                    orders.add(Sort.Order(order0DirEnum, order0Field))
                }
            }
            return PaginationRequestObject(
                draw,
                start,
                if (length == -1) Int.MAX_VALUE else length,
                searchValue,
                if (orders.isEmpty()) defaultSort else Sort.by(orders)
            )
        }
    }
}
