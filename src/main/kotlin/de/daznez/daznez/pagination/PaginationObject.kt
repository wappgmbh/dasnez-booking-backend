package de.daznez.daznez.pagination

/**
 * Created by malte on 16.04.19.
 */
data class PaginationObject<T>(
        var data: List<T>,
        var draw: Int,
        var recordsFiltered: Long,
        var recordsTotal: Long,
        var error: String?
)