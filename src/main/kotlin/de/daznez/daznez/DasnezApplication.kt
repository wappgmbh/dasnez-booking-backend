package de.daznez.daznez

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EnableScheduling
class DasnezApplication

fun main(args: Array<String>) {
	runApplication<DasnezApplication>(*args)
}
