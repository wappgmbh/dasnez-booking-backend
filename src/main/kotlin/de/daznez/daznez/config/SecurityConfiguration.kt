package de.daznez.daznez.config

import de.daznez.daznez.security.LegacyBackendAuthenticationFilter
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy
import org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.web.authentication.HttpStatusEntryPoint
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
class SecurityConfiguration(
    @Value("#{'\${security.allowed-origins}'.split(',')}")
    private val allowedOrigins: List<String>,
    private val legacyBackendAuthenticationFilter: LegacyBackendAuthenticationFilter
): WebSecurityConfigurerAdapter(), WebMvcConfigurer {

    override fun configure(http: HttpSecurity) {
        http.cors().and()
            .csrf().disable()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER).and()
            .authorizeRequests()
            .antMatchers(HttpMethod.GET, "/api/v1/offers").permitAll()
            .antMatchers(HttpMethod.GET, "/api/v1/requests").permitAll()
            .antMatchers(HttpMethod.GET, "/api/v1/districts").permitAll()
            .antMatchers(HttpMethod.GET, "/api/v1/offers/delete/*").permitAll()
            .antMatchers(HttpMethod.GET, "/api/v1/requests/delete/*").permitAll()
            .antMatchers(HttpMethod.POST, "/api/v1/cron/*").permitAll()
            .antMatchers(HttpMethod.GET, "/api/v1/heartbeats").permitAll()
            .anyRequest().authenticated()
            .and()
            .formLogin().disable()
            .httpBasic().disable()
            .logout().disable()
            .exceptionHandling().authenticationEntryPoint(HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED))

        http.addFilterBefore(legacyBackendAuthenticationFilter, UsernamePasswordAuthenticationFilter::class.java)
    }

    override fun addCorsMappings(registry: CorsRegistry) {
        registry.addMapping("/**")
            .allowCredentials(true)
            .allowedHeaders("*")
            .allowedMethods("*")
            .allowedOrigins(*allowedOrigins.toTypedArray())
    }
}