package de.daznez.daznez.config

import de.daznez.daznez.legacy.entity.User
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder
import org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy
import org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.orm.jpa.JpaTransactionManager
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
import org.springframework.transaction.PlatformTransactionManager
import org.springframework.transaction.annotation.EnableTransactionManagement
import javax.persistence.EntityManagerFactory
import javax.sql.DataSource

@Configuration
@EnableJpaRepositories(
    entityManagerFactoryRef = "legacyEntityManager",
    transactionManagerRef = "legacyTransactionManager",
    basePackages = ["de.daznez.daznez.legacy"]
)
@EnableTransactionManagement
class LegacyDbConfiguration {

    @Bean
    @ConfigurationProperties("spring.legacy-datasource")
    fun legacyDataSource(): DataSource {
        return DataSourceBuilder.create().build()
    }

    @Bean(name = ["legacyEntityManager"])
    fun legacyEntityManager(
        builder: EntityManagerFactoryBuilder,
        @Qualifier("legacyDataSource") legacyDataSource: DataSource
    ): LocalContainerEntityManagerFactoryBean {
        return builder
            .dataSource(legacyDataSource)
            .packages(User::class.java)
            .persistenceUnit("legacy")
            .properties(jpaProperties())
            .build()
    }

    @Bean(name = ["legacyTransactionManager"])
    fun legacyTransactionManager(
        @Qualifier("legacyEntityManager") legacyEntityManager: EntityManagerFactory
    ): PlatformTransactionManager {
        return JpaTransactionManager(legacyEntityManager)
    }

    protected fun jpaProperties(): Map<String, String> {
        return mutableMapOf<String, String>(
            "hibernate.physical_naming_strategy" to SpringPhysicalNamingStrategy::class.java.name,
            "hibernate.implicit_naming_strategy" to SpringImplicitNamingStrategy::class.java.name
        )
    }
}