package de.daznez.daznez.config

import de.daznez.daznez.entity.Ad
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder
import org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy
import org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.core.env.Environment
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.orm.jpa.JpaTransactionManager
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter
import org.springframework.transaction.PlatformTransactionManager
import org.springframework.transaction.annotation.EnableTransactionManagement
import javax.persistence.EntityManagerFactory
import javax.sql.DataSource


@Configuration
@EnableJpaRepositories(
    entityManagerFactoryRef = "primaryEntityManager",
    transactionManagerRef = "primaryTransactionManager",
    basePackages = ["de.daznez.daznez.entity", "de.daznez.daznez.repository"]
)
@EnableTransactionManagement
class PrimaryDbConfiguration(
    private val environment: Environment
) {

    @Bean
    fun entityManagerFactoryBuilder(): EntityManagerFactoryBuilder? {
        return EntityManagerFactoryBuilder(HibernateJpaVendorAdapter(), HashMap<String, Any?>(), null)
    }

    @Bean
    @Primary
    @ConfigurationProperties("spring.datasource")
    fun primaryDataSource(): DataSource {
        return DataSourceBuilder.create().build()
    }

    @Bean(name = ["primaryEntityManager"])
    @Primary
    fun primaryEntityManager(
        builder: EntityManagerFactoryBuilder,
        @Qualifier("primaryDataSource") primaryDataSource: DataSource
    ): LocalContainerEntityManagerFactoryBean {
        return builder
            .dataSource(primaryDataSource)
            .packages(Ad::class.java)
            .persistenceUnit("primary")
            .properties(jpaProperties())
            .build()
    }

    @Bean(name = ["primaryTransactionManager"])
    @Primary
    fun primaryTransactionManager(
        @Qualifier("primaryEntityManager") primaryEntityManager: EntityManagerFactory
    ): PlatformTransactionManager {
        return JpaTransactionManager(primaryEntityManager)
    }

    protected fun jpaProperties(): Map<String, String> {
        return mutableMapOf<String, String>(
            "hibernate.physical_naming_strategy" to SpringPhysicalNamingStrategy::class.java.name,
            "hibernate.implicit_naming_strategy" to SpringImplicitNamingStrategy::class.java.name
        ).also {
            if (environment.activeProfiles.contains("test")) {
                it["hibernate.hbm2ddl.auto"] = "create-drop"
            } else {
                it["hibernate.hbm2ddl.auto"] = "validate"
            }
        }
    }
}